<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'status' => 'In three attempts your account is inactive',
    'failed_status' => 'Your account is inactive yet. Please confirm your e-mail address.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];
