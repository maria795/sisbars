<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'failed'   => 'Estas credenciales no coinciden con nuestros registros, al 3 intento su cuenta será bloqueada.',
    'status' => 'Al tercer intento tu cuenta será bloqueada',
    'failed_status' => '¡Su cuenta está inactiva!',
    'throttle' => 'Demasiados intentos de acceso. Por favor intente nuevamente en :seconds segundos.',

];
