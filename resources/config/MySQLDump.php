<?php

/*
 * This file is part of the Laravel Blockchain package.
 *
 * (c) Famurewa Taiwo <famurewa_taiwo@yahoo.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


return [
	/**
	* Blockchain api provided by blockchain.com
	*/
	'db_database' => env('DB_DATABASE'),
	/**
	* This is the default charge fee bitcoin miners at 0.00001
	*/
    'db_username' => env('DB_USERNAME'),
    /*
    * This is your own transaction fee in btc
    */
    'db_password' => env('DB_PASSWORD'),
];
