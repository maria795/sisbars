<div class="modal fade in facture-print"  id="modal-facture" role="dialog" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="col-md-6 id="facture">
          <form><input type="hidden" id="_facture_url" value="{{ url('facture') }}"></form>
          <div class="box-body">
          <div class="space"> </div>
          <div class="ticket" style="background-color: white;">
            <center><span class="company_name"></span></center>
            <center><span class="company_description"></span></center>
            <center><span class="company_datetime"></span></center><BR>
            <div class="data">
              <span class="facture_waiter">MESONERO:</span> <span class="waiter-name"></span><br>
              <span class="facture_waiter">N° ACCIÓN:</span> <span class="action-number"></span><br>
              <span class="facture_waiter">NOMBRE:</span> <span class="customer-name"></span><br>
            </div>
            <table>
            <thead>
              <tr>
                <th class="facture_quantity">CANT-</th>
                <th style="padding-left: 5px" class="facture_product">PRODUCTO</th>
                <th class="facture_price" style="text-align: right;">PRECIO</th>
              </tr>
            </thead>
            <tbody class="main">
            </tbody>
            <tfoot class="tfoot">
            </tfoot>
            </table>
           <center> <p>¡GRACIAS POR SU COMPRA!</p></center>
            <br>
            <input type="hidden" class="iva" name="iva" value="">
            <button type="submit" onclick="print()" class=" oculto-impresion btn btn-block btn-info btn-flat"> IMPRIMIR </button>
        </div>                        
        </div>
      </div> 
    </div>
  </div> 
</div>

