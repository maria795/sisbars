<section class="sidebar">
  @auth
  <ul class="sidebar-menu" data-widget="tree">
    <li class="header" style="color: white; font-size: 1em">MENU</li>
       <li><a href="{{ url('dashboard') }}"><i class="fa fa-cart-plus"></i> <span>Casino</span></a></li>
    <li class="treeview sidebar-menu">
      <ul class="treeview-menu">

    </a></li>
<!-- '''''''''''''''''''''''''''''''''''''' -->
      </ul>
    </li>
    @can('view_users')
    <li><a href="{{ url('customers') }}"><i class="fa fa-users"></i> <span>Clientes</span></a></li>
    <li><a href="{{ url('products') }}"><i class="fa fa-beer"></i> <span>Productos</span></a></li>
    <li><a href="{{ url('tables') }}"><i class="fa fa-th-large"></i> <span>Mesas</span></a></li>
    <li><a href="{{ url('waiters') }}"><i class="fa fa-ship"></i> <span>Mesoneros</span></a></li>
    <li><a href="{{ url('reports') }}"><i class="fa fa-newspaper-o"></i> <span>Reportes de ventas</span></a></li>
    <li><a href="{{ url('backup') }}"><i class="fa fa-user-secret"></i> <span>Copias de seguridad</span></a></li>
    <li><a href="{{ url('configuration') }}"><i class="fa fa-key"></i> <span>Configuración</span></a></li>
    <li class="treeview {{ active_check(['user','login']) }}">
      <a href="#"><i class="fa fa-user"></i> <span>Usuarios</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        @can('view_users')
        <li class="{{ active_check(['user']) }}"><a href="{{ url('user') }}"><i class="fa fa-sort-alpha-desc"></i> Listado</a></li>
        @endcan
        @can('add_users')
        <li class="{{ active_check(['user/create']) }}"><a href="{{ url('user/create') }}"><i class="fa fa-plus-square"></i> Ingresar</a></li>
        @endcan
      </ul>
    </li>
    @endcan
    <li><a href="{{ url('about') }}"><i class="fa fa-share-alt"></i> <span>Acerca de</span></a></li>
  </ul>
  @endauth
</section>
