<div class="modal fade" id="apply_backup_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Confirmar operación</h4>
      </div>
      <div class="modal-body">
    <p><strong><span class="fa fa-trash"></span><center><span style="color:red;">¡ALERTA!</span></center><br> ¿Está seguro de realizar esta acción?</strong></p>
   <p>Sus datos actuales serán eliminado, y volverá a una copia de seguridad antigua </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <a href="#" id="confirm-apply-backup" type="button" class="btn btn-primary"><i id="ajax-icon" class="fa fa-check"></i> Aplicar</a>
      </div>
    </div>
  </div>
</div>
