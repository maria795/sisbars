<div class="modal modal-default fade in" id="modal-danger-product" role="dialog" >
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header" style="background-color: black;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span  style="" aria-hidden="true">×</span></button>
                <h4 class="modal-title text-light" ><strong><center>CONFIRMACIÓN DE OPERACIÓN</center> </strong></h4>
              </div>
              <div class="modal-body">
                <p><center><strong>¿DESEA ELIMINAR DE LA LISTA ESTE PRODUCTO:?</strong></center></p>
                <center><h5><span class="name_product"> </span></h5></center>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn  btn-danger btn-flat  pull-left" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn  btn-danger btn-flat  done_product" data-dismiss="modal" >Confirmar</button>
              </div>
            </div>
      </div>
</div>
