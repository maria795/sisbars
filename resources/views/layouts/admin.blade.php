<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ env('APP_NAME') }} - @yield('title')</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link href="{{ asset('css/buttons.dataTables.min.css') }}" rel="stylesheet" type="text/css">
    @push('scripts')
    <script src="{{ asset('jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/js/dataTables.buttons.js') }}"></script>
    <script src="{{ asset('js/js/jszip.min.js') }}"></script>
    <script src="{{ asset('js/js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/js/buttons.html5.js') }}"></script>
    <script src="{{ asset('js/js/buttons.print.js') }}"></script>
    <script src="{{ asset('js/js/buttons.bootstrap.js') }}"></script> 
    <script src="{{ asset('js/js/vfs_fonts.js') }}"></script> 
    
    @endpush
    <meta name="robots" content="noindex, nofollow">
    </style>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @stack('styles')
    <style type="text/css">

    
    @font-face {
      font-family: 'Source';
      src: url(/fonts/vendor/source-sans-pro/SourceSansPro-Regular.ttf);
    }
    body
    {
      font-family: 'Source' !important;
    }
    }
    .btn-primary
    {
      /*background-color: rgba(1, 60, 163, 255);*/
      background-color : rgba(0, 0, 0, 255);
    }
    .main-sidebar
    {
        background:rgba(0, 0, 0, 255);
    }
    </style>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">
        <a href="{{ url('/') }}" class="logo" style="background-color : rgba(0,0, 0, 255);">
          <span class="logo-mini"><b>S</b>B</span>
          <span class="logo-lg"><b>SIS</b>BAR</span>
        </a>
        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" style="background-color : rgba(0,0, 0, 255);" role="navigation">
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!--<img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">-->
                  <span class="fa fa fa-user"></span>
                  <span class="hidden-xs">{{ Auth::user()->display_name }}</span>
                </a>
                <ul class="dropdown-menu" >
                  <li class="user-header" style="background-color : rgba(0, 0, 10, 255);">
                    <!--<img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">-->
                    <i class="fa fa-user fa-5x" style="color:#fff;"></i>
                    <p>
                      {{ Auth::user()->display_name }}
                      <br>
                      {{ Auth::user()->hasrole('admin') ? 'Administrador' : 'Usuario' }}
                      <small>Miembro desde {{ Auth::user()->created_at->format('d-m-Y') }}</small>
                    </p>
                  </li>
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="{{ url('user', [Auth::user()->encode_id]) }}" class="btn btn-default btn-flat">
                        <i class="fa fa-eye"></i> Datos
                      </a>
                    </div>
                    <div class="pull-right">
                      <a href="logout" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">
                        <i class="fa fa-sign-out"></i> Salir
                      </a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                      </form>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <!-- Uncomment this line to activate the control right sidebar button
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
              -->
            </ul>
          </div>
        </nav>
      </header>

      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar" style="background-color: black;">
        @include('layouts.partials.leftmenu')
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header">
          <h1>
            @yield('page_title')
            <small>@yield('page_subtitle')</small>
          </h1>
          <ol class="breadcrumb">
            @section('breadcrumb')
            @show
          </ol>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
          <!--Page Content Here -->
          @yield('content')
        </section>
      </div>

      <!-- confirm modal -->
      @include('layouts.partials.confirm_modal')
      @include('layouts.partials.confirm_modal_dashboard')
      @include('layouts.partials.confirm_modal_product')
      @include('layouts.partials.confirm_modal_table')
      @include('layouts.partials.confirm_modal_product_edit')
      @include('layouts.partials.confirm_modal_backup')
      @include('layouts.partials.confirm_modal_backup_delete')

      <!-- modal to alert of backup -->
      @include('layouts.partials.apply_backup_modal')
      @include('layouts.partials.facture')


      <!-- Main Footer
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
        </div>
      </footer>
       -->
      <!-- Control Sidebar -->
      <!-- Uncomment this line to activate the control right sidebar menu
      @@include('layouts.partials.sidebar')
      -->
    </div>

    <!-- REQUIRED JS SCRIPTS -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript">
      if ( localStorage.getItem('side_bar_state') == "0" ){
        $('.sidebar-toggle').data('run_from_code', '1');
        //$('.sidebar-toggle').trigger('click');
      }
      else{
        $('.sidebar-toggle').data('run_from_code', '0');
        $('.sidebar-toggle').trigger('click');
      }

      $('.sidebar-toggle').bind('click', function (){
        if ( !localStorage.getItem('side_bar_state') || ( $('.sidebar-toggle').data('run_from_code') == '1' && localStorage.getItem('side_bar_state') == '0' ) ){
          localStorage.setItem('side_bar_state', '1');
        }
        else{
          localStorage.setItem('side_bar_state', '0');
        }
      });
      console.log()
    </script>
    @stack('scripts')
  </body>
</html>
