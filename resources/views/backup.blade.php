@extends('layouts.admin')
@section('title', 'Copia de seguridad')
@section('page_title', 'Copia de seguridad')
@section('page_subtitle', '')
@push('scripts')
<script src="{{ asset('jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('js/admin/backup.js') }}"></script>
@endpush
@section('breadcrumb')
@parent
<li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
<li><a href="{{ url('backup') }}"><i class="fa fa-dashboard"></i> Copia de seguridad</a></li>
<li class="active">Listado</li>
@endsection
@section('content')
<link href="{{ asset('css/waiter.css') }}" rel="stylesheet" type="text/css">

<div class="row">
  <div class="col-md-6">
    <div class="box box-danger" id="box_to_add_backup">
      <div class="box-header">
        <h3 class="box-title">Nueva copia de seguridad</h3>
      </div>
      <div class="box-body">
        <div class="form-group"> 
          <form id="newbackup">
            <input type="hidden" id="_token" value="{{  csrf_token() }}">
            <input type="hidden" id="_url"   value="{{ url('backup') }}"> 
            <div class="form-group">
              <label for="exampleInputEmail1">Nombre de la copia de seguridad</label>
              <div class="input-group">
                <input type="text" name="name"  maxlength="30" autocomplete="off" class="form-control" pattern="[a-zA-Z]{3,}" required>
                <span class="input-group-addon"><i class="fa fa-check"></i></span>
              </div>
            </div>
            <button class="btn btn-block btn-danger btn-flat submit_button" type="submit">Guardar</button>
          </form> 
        </div>
      </div>
    </div>
    <!-- voy aca-->
  </div>

  <!-- /.col (left) -->
  <div class="col-md-6 danger" style="height: 500px;" >
    <div class="box box-danger">
      <div class="box-header">
        <h3 class="box-title">Listado de copias de seguridad</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <form id="main-form">
          <input type="hidden" id="_url" value="{{ url('') }}">
          <input type="hidden" id="_token" value="{{ csrf_token() }}">
          <table class="table table-bordered table-hover table-responsive table-striped display dt-responsive main-table" style="width:100%"  id="table" cellspacing="0">    
            <thead>
             <tr>
              <th>Nombre</th>
              <th>Fecha de creacion</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody class="main-tbody-table" id="table-tbody"></tbody>
        </table>
        </form>
      </div>
    </div>           
  </div>
</div>

@endsection