@extends('layouts.admin')

@section('title', 'Logins')
@section('page_title', 'Logins')
@section('page_subtitle', 'Listado')
@section('breadcrumb')
    @parent
    <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li><a href="{{ url('login') }}">logins</a></li>
    <li class="active">Listado</li>
@endsection
@push('scripts')
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/admin/login/index.js') }}"></script>
@endpush
@section('content')
<link href="{{ asset('css/print.css') }}" rel="stylesheet" type="text/css" media="print">
<link href="{{ asset('css/css.css') }}" rel="stylesheet" type="text/css">
    <section class="content">
      <div class="row">
        <div class="col-md-6">
          <div class="btn-group">
            <button type="submit" class="btn btn-primary printNow"><i class="fa fa-search"> Imprimir</i></button>
          </div>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <div class="box-header logo1" style="display: none;"> <center><img src="/../../../../../../images/header.png"></center> </div>
              <h3 class="box-title">Listado de Logins</h3>
              <div class="box-tools col-sm-12"  >
              <form>
                  <input type="hidden" id="_url" value="{{ url('') }}">
                  <input type="hidden" id="_token" value="{{ csrf_token() }}">
            </form>
              </div>
            </div>
            <div class="box-body table-responsive table-striped">

                <table id="table" class="main-table table table-responsive table-hover">
                 <tbody class="main-tbody-table" id="table-tbody">
                <thead>
                <tr>
                    <th>Usuario</th>
                    <th>Inicio</th>
                    <th>IP</th>
                    <th>Cliente</th>
                  </tr>
                </tr>
              </thead>
               <tfoot>
                    <th>usuario</th>
                    <th>fecha</th>
                    <th style="display: none;"></th>
                    <th style="display: none;"></th>
                </tfoot>
              </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
@endsection
@push('scripts')
@endpush