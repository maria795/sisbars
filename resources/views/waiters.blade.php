@extends('layouts.admin')
@section('title', 'Mesonero')
@section('page_title', 'Listado de mesoneros')
@section('page_subtitle', '')
@push('scripts')
<script src="{{ asset('jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('js/admin/waiter.js') }}"></script>
@endpush
@section('breadcrumb')
@parent
<li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
<li><a href="{{ url('waiters') }}"><i class="fa fa-dashboard"></i> Mesoneros</a></li>
<li class="active">Listado</li>
@endsection
@section('content')
<link href="{{ asset('css/waiter.css') }}" rel="stylesheet" type="text/css">

<div class="row">
  <div class="col-md-6">
    <div class="box box-danger" id="box_to_add_waiter">
      <div class="box-header">
        <h3 class="box-title">Nuevo mesonero</h3>
      </div>
      <div class="box-body">
        <div class="form-group"> 
          <form id="newWaiter">
            <input type="hidden" id="_token" value="{{  csrf_token() }}">
            <input type="hidden" id="_url"   value="{{ url('waiters') }}">
            <div class="form-group">
              <label for="exampleInputEmail1">Nombre</label>
              <div class="input-group">
                <input type="text" name="name"  maxlength="30" autocomplete="off" class="form-control" required>
                <span class="input-group-addon"><i class="fa fa-check"></i></span>
              </div>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Porcentaje</label>
               <div class="input-group">
                  <input type="number" step="any" autocomplete="off" name="percentage" class="form-control" min="0" max="100">
                  <span class="input-group-addon"><i class="fa fa-check"></i></span>
              </div>
            </div>
            <button class="btn btn-block btn-danger btn-flat submit_button" type="submit">Guardar</button>
          </form> 
        </div>
      </div>
    </div>
    <!-- voy aca-->

    <div class="box box-danger" id="box_to_edit_waiter" style="display: none;">
      <div class="box-header">
        <h3 class="box-title">Editar mesonero</h3>
      </div>
      <div class="box-body">
        <div class="form-group"> 
          <form id="editWaiter">
            <input type="hidden" id="_token" value="{{  csrf_token() }}">
            <input type="hidden" id="_url"   value="{{ url('waiters') }}">

              <div class="form-group">
                <label for="exampleInputEmail1">Nombre</label>
                <div class="input-group">
                  <input type="hidden" id="item_id">
                  <input type="hidden" name="id" id="edit_id">
                  <input type="text" name="name" maxlength="30" class="form-control name-edit" required>
                  <span class="input-group-addon"><i class="fa fa-check"></i></span>
                </div>
              </div>
              <div class="form-group">
               <label for="exampleInputEmail1">Porcentaje</label>
               <div class="input-group">
                 <input type="number" step="any" name="percentage" class="form-control percentage-edit" min="0" max="100">
                 <span class="input-group-addon">.00</span>
               </div>
             </div>
             <button class="btn btn-block btn-danger btn-flat submit_button" type="submit">Guardar</button>
             <button class="btn btn-block btn-danger btn-flat" type="button" id="cancel_edit">Cancelar</button>
          </form> 
        </div>
     </div>
    </div>
  </div>

  <!-- /.col (left) -->
  <div class="col-md-6 danger" style="height: 500px;" >
    <div class="box box-danger">
      <div class="box-header">
        <h3 class="box-title">Mesonero disponibles</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <form id="main-form">
          <input type="hidden" id="_url" value="{{ url('') }}">
          <input type="hidden" id="_token" value="{{ csrf_token() }}">
          <table id="table" class="main-table table table-bordered table-striped display dt-responsive" style="width:100%">
            <thead>
             <tr>
              <th>Nombre</th>
              <th>Porcentaje</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody class="main-tbody-table" id="table-tbody"></tbody>
        </table>
        </form>
      </div>
    </div>           
  </div>
</div>

@endsection