@extends('layouts.adminfront')

@section('title', 'Recuperar contraseña')

@section('content')
    <div class="login-box">
      <div class="login-logo">
        <a href="{{ url('/') }}"><b>SIS</b>GEIN</a>
      </div> 
      <div class="login-box-body">
        <p class="login-box-msg"><b>Recuperar contraseña</b></p>
        <p class="login-box-msg">Debe recordar la respuesta de su pregunta secreta, si no, pregúntele al administrador del sistema.</p>

        <form id="main-form1" >
          <input type="hidden" id="_url" value="{{ url('response') }}">
          <input type="hidden" id="_token" value="{{ csrf_token() }}">
          <input type="hidden" id="_redirect" value="{{ url('/') }}">


          <div class="form-group has-feedback email">
            <input type="email" class="form-control" id="email" name="email" placeholder="Correo electrónico">
            <span class="fa fa-envelope form-control-feedback"></span>
            <span class="missing_alert text-danger" id="email_alert"></span>
          </div>

          <div class="row email">
            <div class="col-xs-12">
              <button type="submit"  class="btn btn-primary btn-block ajax" >
                <i id="ajax-icon" id="registro" class="fa fa-envelope"></i> Recuperar
              </button>
            </div>
          </div>
        </form>

        <form id="main-form2" method="POST">

        <input type="hidden" id="_url" value="{{ url('password/email') }}">
        <input type="hidden" id="_token" value="{{ csrf_token() }}">
          <input type="hidden" id="_redirect" value="{{ url('/') }}">       

          <div id="question" style="display: none;"> 

            <input type="hidden" class="form-control" id="email" name="email">

              <div class="form-group has-feedback">
                <p class=""><b>Su pregunta es:</b> <span class="question-1"> </span></p>
                <input type="text" class="form-control" id="answer" name="answer" placeholder="Introduzca su respuesta secreta">
                <span class="missing_alert text-danger" id="answer_alert"></span>

              </div>

            <div class="row">
                <div class="col-xs-12">
                  <button type="submit" class="btn btn-primary btn-block ajax" >
                    <i id="ajax-icon" id="verification" class="fa fa-envelope"></i> Comprobar
                  </button>
                </div>
              </div>
          </div>
        </form>
      </div>
    </div>

@endsection

@push('scripts')
    <script src="{{ asset('js/admin/auth/answer.js') }}"></script>
@endpush