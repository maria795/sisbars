@extends('layouts.admin')
@section('title', 'Reportes')
@section('page_title', 'Reporte de ventas')
@section('page_subtitle', '')
@section('breadcrumb')
@push('scripts')
<script src="{{ asset('js/admin/reports.js') }}"></script>
<script src="{{ asset('js/facture.js') }}"></script>
<script src="{{ asset('js/jquery.easy-autocomplete.min.js') }}"></script>
<script src="{{ asset('js/admin/reports-date.js') }}"></script>
@endpush
@parent
    <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li><a href="{{ url('reports') }}">Reporte de venta</a></li>
    <link href="{{ asset('css/style_facture.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('css/facture.css') }}" rel="stylesheet" type="text/css" media="print">
    <link href="{{ asset('css/easy-autocomplete.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/easy-autocomplete.themes.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/reports.css') }}" rel="stylesheet" type="text/css">
    <li class="active">Listado</li>
@endsection
@section('content')
<section>
<div class="row">
    <form id="workOrder">
          <input type="hidden" id="_token" value="{{  csrf_token() }}">
          <input type="hidden" id="_url"   value="{{ url('data') }}">
          <input type="hidden" id="_url1"   value="{{ url('') }}">
          <input type="hidden" id="_url5"   value="{{ url('dataRange')}}">
    </form>
        <div class="col-md-8">
         <div class="box box-info table_delete">
            <div class="box-header with-border">
              <h3 class="box-title">Ventas</h3>
              <div class="box-tools pull-right">
                <!--<button style="color: white;" class="btn btn-info btn-block printNow btn-info btn-flat submit_button btn-box-tool" type="button" >IMPRIMIR</button> -->
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class=""></i>
                </button>
              </div>
            </div>
             <div class="box-body" style="background-color: white;">
            <div class="row"> 
              <div class="col-md-12 pdfButton"></div>
            </div>
              <div class="row" > 
              <div class="col-md-3 datatables_length1">
                 Desde: <input type="text" class="form-control" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" placeholder="DD-MM-AAAA" type="date" class="aactivate-advanced-modal" id="fini1" name="min">
              </div>

              <div class="col-md-3 datatables_length1">
                 Hasta :<input type="text" class="form-control" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" placeholder="DD-MM-AAAA" class="aactivate-advanced-modal" type="date" id="ffin1" name="max">
              </div>
            </div>
              <div class="col-md-6 datatables_length1">
              </div>
            <hr>

            <div class="row"> 
              <div class="col-md-6 dataTables_length1"></div>
              <div class="col-md-6 dataTables_filter1"></div>
            </div>

             <table  class="table table-bordered table-hover table-responsive table-striped display dt-responsive main-table" style="width:100%"  id="table" cellspacing="0">
             <tbody class="main-tbody-table" id="table-tbody">
             <thead>
                <tr>
                    <th>Fecha</th>
                    <th>Estado</th>
                    <th>N° Mesa</th>
                    <th>Mesero</th>
                   <!-- <th>Productos</th> -->
                    <th>Comentario</th>
                    <th>Total</th>
                    <th>Comisión</th>
                    <th>Acciones</th>

                </tr>
            </thead>
            <tfoot>
                  <tr>
                      <th class="foot"></th>
                      <th class="foot"></th>
                      <th class="foot"></th>
                      <th class="foot"></th>
                      <th class="foot"></th>
                      <!--<th></th>-->
                      <th class="foot"></th>
                      <th class="foot"></th>
                      <th style="display: none;"></th>
                  </tr>
            </tfoot>
            </tbody>
        </table>
      </div>
    </div>            
    <!--de aqui -->
            <div class="box box-info table_delete">
            <div class="box-header with-border">
              <h3 class="box-title">Ventas de producto por rango de fecha</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class=""></i>
                </button>
              </div>
            </div>
             <div class="box-body" style="background-color: white;">
            <div class="row"> 
              <div class="col-md-12 pdfButton"></div>
            </div>

            <div class="row" > 
              <div class="col-md-6 datatables_length1">
                 Buscar:
                 <div class="input-group input-group-xs">
                    <input type="text" id="input_product" placeholder="Buscar producto" class="form-control" style="height: 35px">
                    <span class="input-group-btn">
                      <button type="submit" class="btn waves-effect bg-danger btn-flat submit_button"><span class="fa fa-eye"> </span></button>
                    </span>
                  </div>
              </div>

              <div class="col-md-3 datatables_length1">
                 Desde: <input type="text" class="form-control" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" placeholder="DD-MM-AAAA" type="date" class="aactivate-advanced-modal" id="fini" name="min">
              </div>

              <div class="col-md-3 datatables_length1">
                 Hasta :<input type="text" class="form-control" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" placeholder="DD-MM-AAAA" class="aactivate-advanced-modal" type="date" id="ffin" name="max">
              </div>
            </div>
            <hr>
            <!-- Aca-->
              <div class="active"> 
               <table  class="table-range table table-bordered-range table-hover table-responsive table-striped display dt-responsive main-table" style="width:100%"  id="table-range" cellspacing="0">
                   <tbody class="main-tbody-table-range" id="table-tbody-range">
                   <thead>
                      <tr>
                          <th class="t-trigger">Fecha</th>
                          <th>Producto</th>
                          <th>Cantidad</th>
                          <th>Precio actual</th>
                          <th>Total</th>
                          <th></th>
                      </tr>
                  </thead>
                  <tfoot>
<!--                       
                      <tr>
                          <th colspan="4"> </th>
                          <th><span style="text-align: left; font-weight: bold;">1000</span></th>
                      </tr>
 -->
                  </tfoot>
                  </tbody>
                </table>
              </div>
        <!-- Fin-->
        </div>
    </div>         

    <!-- hasta -->
  </div>
  <div class="col-md-4 delete">
      <div class="box box-info delete">
            <div class="box-header with-border">
              <h3 class="box-title">  </h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool event" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
            <table id="example1" class="table table-bordered table-striped display dt-responsive">
                <tbody>
                <tr scope="col" class="sale"> 
                    <td colspan="2"> <span class="company_name"></span></td>
                </tr>
                <tr scope="col" class="sale"> 
                    <td colspan="2"> <span class="company_description"> </span></td>
                </tr>
                <tr> 
                    <td colspan="2"><center><span  class="company_datetime sale1"> </span></center></td> 
                </tr>
                <tr scope="col"> 
                    <td> <span class="sale"> CLIENTE </span></td>
                    <td> <span class="customer-sale">  </span></td>
                </tr>
                <tr scope="col"> 
                    <td> <span  class="sale"> MESONERO</span></td>
                    <td> <span class="waiter-sale"> </span><input type="hidden" class="percentage" name="waiter_percentage" value="">
                </td>
                </tr>
                <tr class="table-danger"> 
                    <td colspan="2"><span class="sale">  ADICIONES</span></td>
                </tr>
                <tr> 
                  <td colspan="2"> 
                    <div class="row ">
                      <div class="col-xs-4 ">
                          <span class="title-quantity"> CANTIDAD </span>
                      </div>
                      <div class="col-xs-8">
                          <span class="title-quantity"> DESCRIPCION </span>              
                      </div>
                    </div>                           
                    <div class="row" id="additional_product">
                    </div>
                  </td>
                </tr>
                  <tr> 
                    <td colspan="2"> <button type="button" data-target="#modal-facture" id="facture_id" value="" data-toggle="modal" class="btn btn-block btn-info btn-flat">REEIMPRIMIR FACTURA </button></td>
                  </tr>  
                </tbody>
            </table>
          </div>
         </div>           
      </div>
  <!-- end-->
    </div>
</section>
@endsection
@push('scripts')
@endpush

