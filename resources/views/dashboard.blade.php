@extends('layouts.admin')
@section('title', 'Inicio')
@section('page_title', 'Inicio')
@section('page_subtitle', 'Principal')
@push('scripts')
<script src="{{ asset('js/reservation.js') }}"></script>
<script src="{{ asset('js/facture.js') }}"></script>
<script src="{{ asset('js/jquery.easy-autocomplete.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-toggle.js') }}"></script>

@endpush
@section('breadcrumb')
@parent
<li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
<li class="active">Principal</li>
@endsection
@section('content')
<link href="{{ asset('css/reservation.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/style_facture.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/easy-autocomplete.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/easy-autocomplete.themes.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('css/bootstrap-toggle.css') }}" rel="stylesheet" type="text/css">

<style type="text/css">
  
.easy-autocomplete-container{
  background-color: white !important;
  width: 200% !important; 
  z-index : 4;
}

.toggle.android { border-radius: 0px !important; margin-top: -1px !important;  height: 34px !important;  }
.toggle.android .toggle-handle { border-radius: 0px  !important; margin-top: -1px !important;  height: 38px !important; }

</style>

<link href="{{ asset('css/facture.css') }}" rel="stylesheet" type="text/css" media="print">
<section>
  <div class="col-md-14">
    <div class="box box-solid">
      <div class="box-body">
        <div class="row">
          <!-- Left col -->
          <section class="col-lg-6 connectedSortable part1">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs pull-right">
                <li class="active"><a href="#revenue-chart" data-toggle="tab">Registrar</a></li>
                <li><a href="#sales-chart" data-toggle="tab" id="reservation">Reservaciones</a></li>
                <li class=""><a href="#product" data-toggle="tab" id="toggle_move_reservation">Mudanza</a></li>

                <li class="pull-left header"><i class="fa fa-inbox"></i> Reservacion</li>
              </ul>
              <!-- here register-->
              <!-- inicio registrar orden-->
              <div class="tab-content no-padding display">
                <div class="product tab-pane " id="product" style="position: relative; height: 400px;">
                  <div class="form-group">
                    <form  id="moveWorkOrder" >
                      <input type="hidden" id="_url" value="{{ url('') }}">
                      <input type="hidden" id="_url1" value="{{ url('') }}">

                      <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                      <input type="hidden" id="_url2" value="{{ url('dashboard') }}">
                      <input type="hidden" name="current_action_number" class="current_action_number" value="{{ url('dashboard') }}">

                      <input type="hidden" name="customer_id" class="customer_id" value="">
                      <div class="row ">
                        <div class="col-xs-4">
                          <label class="title-input">MESA</label>
                          <select class="form-control table_dropdown" name="table_id" required>
                            <option></option>
                          </select>                
                        </div>
                        <div class="col-xs-4">
                          <label class="title-input">MESONERO</label>
                            <select class="form-control waiter_dropdown" name="waiter_id" required>
                            </select>
                          </div>

                          <div class="col-xs-4">
                            <label class="title-input">N° ACCIÓN</label>
                            <input type="text" class="form-control action_number action_number_dash" autocomplete="off" name="action_number" placeholder="" required> 
                          </div>
                        </div>
                        <div class="row">
                        <div class="col-xs-6">
                          <div class="form-group">
                            <label class="title-input">NOMBRE</label>
                            <input type="text" class="form-control name customerSearch flat" style="width: 170%;" id="customerinput" autocomplete="off" name="name" required>
                          </div>
                        </div>
                        <div class="col-xs-6">
                            <label class="title-input">DIRECCIÓN</label>
                            <input type="text" class="form-control direction" name="direction">
                        </div>
                        </div>
                        <div class="row">       
                            <div class="col-xs-6">
                              <label class="title-input">TELÉFONO</label>
                              <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-phone"></i>
                                </div>
                                <input type="number" class="form-control phone" name="phone" data-inputmask="&quot;mask&quot;: &quot;(999) 999-9999&quot;" data-mask="">
                              </div>
                            </div>
                            <div class="col-xs-6">                              
                              <label class="title-input">COMENTARIO</label>
                              <input type="text" autocomplete="off" class="form-control description" maxlength="20" name="description">
                            </div>
                        </div>

                        <!--- Adicionar-->
                        <div id="services_to_move" style="overflow: auto; height: 150px;" > 
                          <!-- No touchs-->
                          <div class="row col-xs-12">
                            <div class="col-xs-2"></div>
                            <div class="col-xs-2 a">
                              <label>CANTIDAD</label>
                            </div>
                            <div class="col-xs-6 a">
                              <label>DESCRIPCIÓN</label>      
                            </div>
                            <div class="col-xs-2 a">
                              <label>PRECIO</label>
                            </div>
                          </div>
                          <!-- -->
                          <!-- PRODUCT ORIGINAL-->
                          <div class="row col-xs-12 product" id="original_services_to_Move">
                            <div class="col-xs-2 ">
                             <input type="checkbox" class="service_id" name='services[]' value="" checked>
                            </div>

                            <div class="col-xs-2 ">
                              <label class="quantity"></label>
                            </div>
                            <div class="col-xs-6 ">
                              <label class="product_name"></label>      
                            </div>
                            <div class="col-xs-2 ">
                              <label class="price"></label>
                            </div>
                          </div>
                      </div>

                      <div class="row">
                        <div class="col-xs-6">
                          <button type="submit" class="btn btn-block btn-info btn-flat submit_button">ABRIR MESA</button>
                        </div>
                        <div class="col-xs-6">
                          <button type="button" class="btn btn-block btn-info btn-flat" id="cancel_move">CANCELAR MUDANZA</button>
                        </div>
                      </div>
                    </form>  
                    <!-- fin registrar orden-->
                  </div>
                </div>



                <!--_____________________________________________________________________________________________________________-->

                <div class="chart tab-pane active display" id="revenue-chart" style="position: relative; height: 400px;">
                  <div class="form-group">
                    <form  id="newWorkOrder" >
                      <input type="hidden" id="_url" value="{{ url('') }}">
                      <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                      <input type="hidden" id="_url2" value="{{ url('dashboard') }}">
                      <input type="hidden" name="current_action_number" class="current_action_number" value="">

                      <input type="hidden" name="customer_id" id="customer_id" value="">
                      <div class="row ">
                        <div class="col-xs-2">
                          <label class="title-input" style="font-size:0.7em ">VENTA RAPIDA</label>
                           <input type="checkbox" value="0" name="sales" id="toggle-event" pattern="((?!((&[^\ ]*;))|([<>])).)*" data-toggle="toggle" data-style="android">             
                        </div>

                        <div class="col-xs-2">
                          <label class="title-input">MESA</label>
                          <select class="form-control table_dropdown" name="table_id" required>
                            <option></option>
                          </select>
                        </div>
                        <div class="col-xs-4">
                          <label class="title-input">MESERO</label>
                          <select class="form-control waiter_dropdown" name="waiter_id" required>
                            <option></option>
                          </select>                
                        </div>

                        <div class="col-xs-4">
                          <label class="title-input">N° DE ACCIÓN</label>
                          <input type="text" class="form-control action_number" autocomplete="off" name="action_number" placeholder="" required> 
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="title-input">NOMBRE</label>
                        <input type="text" class="form-control name flat" id="inputcustomer" autocomplete="off" name="name" required>

                      </div>
                      <div class="form-group">
                        <label class="title-input">DIRECCIÓN</label>
                        <input type="text" class="form-control direction" name="direction">
                      </div>
                      <div class="row ">
                      <div class="form-group col-xs-6">
                        <label class="title-input">TELÉFONO</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-phone"></i>
                          </div>
                          <input type="number" class="form-control phone"  name="phone" data-inputmask="&quot;mask&quot;: &quot;(999) 999-9999&quot;" data-mask="">
                        </div>
                      </div>
                      <div class="form-group  col-xs-6">
                        <label class="title-input">COMENTARIO</label>
                          <input type="text" autocomplete="off" class="form-control description" maxlength="20" name="description">
                      </div>
                   </div>
                      <button type="submit" class="btn btn-block one btn-info btn-flat submit_button">ABRIR MESA</button>
                    </form>
                    <!-- fin registrar orden-->
                  </div>
                </div>


                <!-- end here register-->
                <!-- here reservation-->

                  <div class="chart tab-pane display" id="sales-chart" style="position: relative; height: 280;">
                    <form id='newService'>
                      <div class="table-responsive">
                        <table class="table">
                          <tr class="row1" style="height: 10px;"> 
                            <td>
                              <select name="table_id" readonly="" class="form-control table_dropdown" id="change_work_order">
                                <option></option>
                              </select>
                              <span class="table_number_color">MESA N°</span>
                              <span class="table_number_color table_id"></span>
                            </td>
                            <td></td>
                            <td></td>
                            <td style="text-align: right; padding: 3px;">
                              <button type="button" data-target="#modal-danger-dashboard"  data-toggle="modal"  style="margin:0px;" class="btn bg-danger disa waves-effect btn-flat" id="close_work_order_modal">CERRAR</button>
                            </td>
                          </tr>
                          <tr> 
                            <td class="title"> N° ACCIÓN </td>
                            <td> <span class="action_number "> </span></td>
                            <td class="title"> MESERO </td>
                          <!--Agregado input -->
                          <td>  
                            <span class="name_waiter"></span>
                            <span>
                              <select class="form-control waiter_dropdown" id="change_waiter">
                              </select>
                            <span>
                          </td>
                          </tr>
                          <!--Agregado input -->
                          <tr>
                            <td class="title"> CLIENTE </td>
                            <td> <span class="name_customer"> </span></td>
                            <td class="title"> TELEFONO </td>
                            <td> <span class="phone"> </span></td>
                          </tr>
                          <tr>
                            <td class="title"> COMENTARIO </td>
                            <td colspan="3"> <span class="description_work_order"></span>
                                <input type="hidden" value="" class="sales" name="sales">
                            </td>
                          </tr>

                        <!--_____________________Adicionar clientes_______________________ -->

                        <table class="table table display">  
                            <input type="hidden" id="_service_url" value="{{ url('services') }}">
                            <input type="hidden" id="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="work_order_id" id="work_order_id" value="">
                            <input type="hidden" id="code" value="123">
                            <input type="hidden" id="table_customer_id" value="">
                            <!--AGREGADO-->
                            <input type="hidden" class="percentage" name="waiter_percentage" value="">
                            <!--AGREGADO-->

                            <tr class="title">
                              <td>
                                  <div class="row">   
                                  <div class="col-xs-8">
                                    <input name="product" id="input_product" placeholder="Producto"  class="form-control  disabled flat" style="width: 120%;" aria-hidden="true"> 
                                    <input type="hidden" name="product_id" id="product_id">
                                  </div>

                                  <div class="col-xs-4">
                                    <input type="number" step="any" autocomplete="off" name="quantity" value="1" placeholder="Cantidad"  id="product_quantity" class="form-control disabled " min="1" style="width: 120%;">
                                  </div>
                                  </div>
                              </td>

                              <td>                     
                                    <input type="text" autocomplete="off" title="Comentario" step="any" placeholder="Comentario" maxlength="10" name="description" id="product_description" class=" disabled form-control">
                              </td>

                               <td width="160">                  
                                <div class="input-group input-group-xs">
                                  <input type="text" id="product_total" placeholder="Total" class="form-control disabled" readonly>
                                  <span class="input-group-btn">
                                    <button type="submit" class="btn waves-effect bg-danger btn-flat submit_button disa"><span class="fa fa-plus" > </span></button>
                                  </span>
                                </div>
                              </td>
                            </tr>


                            <tr>
                              <td colspan="4">
                                <div style="overflow: auto; height: 120px;" id="services"> 
                                  <!-- No touchs-->
                                  <div class="row col-xs-12 additional">
                                    <div class="col-xs-3">
                                    </div>

                                    <div class="col-xs-2 a">
                                      <label class="product_id">CANTIDAD</label>
                                    </div>
                                    <div class="col-xs-3 a">
                                      <label class="product_name">DESCRIPCIÓN</label>      
                                    </div>
                                    <div class="col-xs-2 a">
                                      <label>PRECIO</label>
                                    </div>
                                    <div class="col-xs-2 a">
                                      <label>TOTAL</label>
                                    </div>
                                  </div>
                                  <!-- -->
                                  <div class="row col-xs-12 product" id="original_product" style="display: none">
                                    <div class="col-xs-3 ">
                                      <button type="button" style="padding: 3px; margin-left: 4px;" class="btn disa btn-danger btn-sm description"  data-toggle="tooltip"  title="" data-original-title="">
                                        <i class="fa  fa-commenting-o"></i>
                                      </button>
                                      <input type="text" style="display: none;" step="any" class="comment"/>

                                      <button type="button" style="padding: 3px; margin-left: 4px;" class="btn btn-danger btn-sm delete modal_product disa" data-toggle="modal" title="Eliminar producto" data-target="#modal-danger-product" id="confirm_delete_product" data-original-title="Eliminar servicio">
                                        <i class="fa fa-times"></i>
                                      </button>

                                      <button type="button" style="padding: 3px;" class="btn btn-danger btn-sm edit disa" data-toggle="modal" data-target="#modal-danger-product_edit" title="Editar producto" data-original-title="editar servicio">
                                        <i class="fa fa-edit"></i>
                                      </button>

                                      <button type="button" style="padding: 3px;" class="btn btn-success btn-sm accept" data-toggle="tooltip" title="Aplicar" data-original-title="Aplicar">
                                        <i class="fa fa-check"></i>
                                      </button>
                                    </div>

                                    <div class="col-xs-2 ">
                                      <input type="number"  autocomplete="off" step="any" class="quantity" min="1" />
                                      <label class="quantity"></label>
                                    </div>
                                    <div class="col-xs-3 ">
                                      <label class="product_name"></label>      
                                    </div>
                                    <div class="col-xs-2 ">
                                      <label class="price"></label>
                                    </div>
                                    <div class="col-xs-2 ">
                                      <label class="total"></label>
                                    </div>
                                  </div>

                                </div>
                              </td>
                            </tr>
                              <tr class="title">
                                <td colspan="4">
                                 <div class="row">
                                  <div class="col-xs-9">
                                    <span class="subtotal">SUBTOTAL </span>
                                  </div>
                                  <div class="col-xs-3 ">
                                    <span class="subtotal" id="subtotal"> 0 </span>
                                  </div>
                                  <div class="col-xs-9">
                                    <span class="iva">IVA </span>
                                  </div>
                                  <div class="col-xs-3 ">
                                    <span class="iva" id="iva"> 0 </span>
                                  </div>                                  
                                  <div class="col-xs-9">
                                    <span class="service_total">SERVICIO </span>
                                  </div>
                                  <div class="col-xs-3 ">
                                    <span class="service_total" id="service_total"> 0 </span>
                                  </div>
                                  <div class="col-xs-9">
                                    <span class="total">A PAGAR </span>
                                  </div>
                                  <div class="col-xs-3 ">
                                    <span class="total apagar" id="total"> 0 </span>
                                  </div>
                                </div>
                              </td>
                            </tr> 

                            <!--sas -->
                            <tr  class="title">
                              <td width="550">
                                <button type="button" class="btn bg-danger waves-effect disa btn-flat toggle_change_waiter"><span class="fa fa-close"></span> CAMBIAR MESA</button>
                                <button type="button" class="btn bg-danger waves-effect disa btn-flat toggle_change_waiter"><span class="fa fa-close"></span>CAMBIAR MESONERO</button>
                              </td>
<!--                               <td>
                              </td>
 -->                              <td>
                                <button type="button" class="btn bg-danger waves-effect disa btn-flat" id="moveServices">
                                  <span class="fa  fa-exchange"></span>MUDAR CLIENTE
                                </button>
                              </td>
                              <td style="text-align: right;">
                                <button type="button" class="btn bg-danger waves-effect disa btn-flat" value="" data-target="#modal-facture"  data-toggle="modal" id="facture_id_work" ><span class="fa fa-file-text-o"></span> FACTURA </button>
                              </td>
                            </tr> 
                            <!-- -->

                            </form> 
                          </table>
                        </div>
                      </div>
                </form>
              </div>
            </div>
          </section>   

          <section class="col-lg-6 connectedSortable list display">
            <div class="box header">
              <div style="display: none; justify-content: center; height: 80px;">
                <div class="row">
                  <div class="col-sm-4 col-xs-6">
                    <div style="" class="description-block border-right">
                      <span class="description-percentage text-green" style="font-size:24px;"> <strong>40</strong></span><br>
                      <span style="font-size:10px;"> DISPONIBLES</span>
                    </div>
                  </div>

                  <div class="col-sm-4 col-xs-6">
                    <div style="" class="description-block border-right">
                      <span class="description-percentage text-yellow" style="font-size:24px;"> <strong>40</strong></span><br>
                      <span style="font-size:10px;"> OCUPADAS</span>
                    </div>
                  </div>


                  <div class="col-sm-4 col-xs-6">
                    <div style="" class="description-block border-right">
                      <span class="description-percentage text-red" style="font-size:24px;"> <strong>40</strong></span><br>
                      <span style="font-size:10px;"> POR PAGAR</span>
                    </div>
                  </div>
                </div>
                <!-- /.row -->
              </div>

              <div class="box-header with-border parte3 title">
                <div class=" col-xs-8">
                  <h4 class="box-title">Mesas activas</h4>
                </div>
                <div class=" col-xs-4">
                  <div id="table_filter" class="dataTables_filter">
                    <input type="search" class="form-control input-sm" placeholder="BUSCAR" aria-controls="table" id="search" autocomplete="off">
                  </div>              
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body col-xs-12 table-facture" style="overflow-y: auto; overflow-x: hidden; height: 440px;">
                <div class="row" id="tables">
                  <div class="col-xs-4" style="margin: 0%; display: none;" id="original_table">
                    <div class="info-box">
                     <!-- <div style="display: flex; justify-content= space-between; align-content: space-between ">-->
                          <span class="info-box-icon">
                            <span class="id_table-div customer_action_number">N/A</span>
                            <span class="id_table-div customer_name"></span>
                          </span>
                     <!-- </div>      -->            
                      <div class="progress" style="width: 99%; margin-left: 1%">
                        <div class="progress-bar" style="width: 0%"></div>
                      </div>
                      <div class="info-box-content" style="padding: 0px">
                        <span class="info-box-text" style="text-align: center;float: left;margin: 5%">MESA</span>
                        <inpu class="info-box-number table_number" style="float: right;font-size: 1.5em;margin-right: 5%;">10</span>
                      </div>
                    </div>
                  </div>
              </div>
                <!-- /.row -->
              </div>
              <!-- ./box-body -->
              <!-- /.box-footer -->
            </div>
            <!-- /.box -->
          </section>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">
 function activeICheck(){
    $('input').not('#toggle-event').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
    });
  }
  activeICheck();
</script>
@endpush
