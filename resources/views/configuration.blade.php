@extends('layouts.admin')
@section('title', 'configuración')
@section('page_title', 'Listado de configuración')
@section('page_subtitle', '')
@push('scripts')
<script src="{{ asset('js/admin/configuration.js') }}"></script>
@endpush
@section('breadcrumb')
@parent
<li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
<li><a href="{{ url('/configuration') }}"><i class="fa fa-dashboard"></i> Configuración</a></li>
<li class="active">Listado</li>
@endsection
@section('content')
<div class="row">
<div class="col-md-6 none">
  <div class="box box-danger" id="box_to_add_desktop">
      <div class="box-header">
        <h3 class="box-title">Configuración de ticker</h3>
      </div>
      <div class="box-body">
        <div class="form-group"> 
          <form id="editConfiguration">
            <input type="hidden" id="_token" value="{{  csrf_token() }}">
            <input type="hidden" id="_url"   value="{{ url('configuration') }}">
            <input type="hidden" id="edit_id" value="">
            <div class="form-group">
              <label for="exampleInputEmail1">Nombre de la empresa</label>
              <div class="input-group">
                <input type="text" name="name" class="form-control name" required>
                <span class="input-group-addon"><i class="fa fa-check"></i></span>
              </div>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Descripción</label>
              <div class="input-group">
                <input type="text" name="description" class="form-control description" required>
                <span class="input-group-addon"><i class="fa fa-check"></i></span>
              </div>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">I.V.A</label>
              <div class="input-group">
                <input type="text" name="iva" class="form-control iva" required>
                <span class="input-group-addon"><i class="fa fa-check"></i></span>
              </div>
            </div>
            <div class="checkbox icheck">
              <label for="exampleInputEmail1" >COBRO DE SERVICIO</label><br><br>
                <input type="radio" class="status-active" name="status"  id="radio" value="SI" checked="">SI&nbsp;&nbsp;
                <input type="radio" class="status-inactive" name="status" id="radio"  value="NO"> NO
              <span class="missing_alert text-danger" id="radio_alert"></span>
           </div>
            <button class="btn btn-block btn-danger btn-flat submit_button" type="submit">Guardar</button>
          </form> 
    </div>
    </div>
</div>
</div>
</div>
@endsection

@push('scripts')
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
  });
</script>
@endpush

