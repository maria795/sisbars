@extends('layouts.admin')
@section('title', 'Clientes')
@section('page_title', 'Listado de clientes')
@section('page_subtitle', '')
@push('scripts')
<script src="{{ asset('js/admin/customer.js') }}"></script>
@endpush
@section('breadcrumb')
@parent
<li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
<li class="active">Principal</li>
@endsection
@section('content')
<link href="{{ asset('css/customer.css') }}" rel="stylesheet" type="text/css">

<div class="row">
  <div class="col-md-6">
    <div class="box box-danger" id="box_to_add_customer">
      <div class="box-header">
        <h3 class="box-title">Nuevo cliente</h3>
      </div>
      <div class="box-body">
        <div class="form-group"> 
         <form id="newCustomer">
          <input type="hidden" id="_token" value="{{  csrf_token() }}">
          <input type="hidden" id="_url"   value="{{ url('customers') }}">
          <!-- Comienza aca-->
          <div class="form-group">
            <label>Número de acción</label>
            <input type="text" name="action_number" maxlength="5" class="form-control" required>
          </div>
          
          <div class="form-group">
            <label for="exampleInputEmail1">Nombre</label>
            <input type="text" name="name" class="form-control" maxlength="30" placeholder="" required>
          </div>

          <div class="form-group">
            <label>Dirección</label>
            <input type="text" name="direction" class="form-control">
          </div>
          
          <div class="form-group">
            <label>Teléfono</label>
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-phone"></i>
              </div>
              <input type="text" class="form-control" name="phone" data-inputmask="&quot;mask&quot;: &quot;(999) 999-9999&quot;" data-mask="">
            </div>
          </div>
          <!-- Comienza aca-->

          <button class="btn btn-block btn-danger btn-flat submit_button" type="submit">Guardar</button>
        </form> 
      </div>
    </div>
  </div>

  <div class="box box-danger" id="box_to_edit_customer" style="display: none;">
    <div class="box-header">
      <h3 class="box-title">Editar cliente</h3>
    </div>
    <div class="box-body">
      <div class="form-group"> 
        <form id="editCustomer">
          <input type="hidden" id="_token" value="{{  csrf_token() }}">
          <input type="hidden" id="_url"   value="{{ url('customers') }}">

          <div class="form-group">
            <label>Número de acción</label>
            <input type="hidden" id="item_id">
            <input type="hidden" name="id" id="edit_id">
            <input type="text" name="action_number" minlength="0" maxlength="5" class="form-control action_number-edit" required>
          </div>
          
          <div class="form-group">
            <label for="exampleInputEmail1">Nombre</label>
            <input type="text" name="name" class="form-control name-edit" minlength="0" maxlength="30" placeholder="" required>
          </div>

          <div class="form-group">
            <label>Dirección</label>
            <input type="text" name="direction" maxlength="30" class="form-control direction-edit">
          </div>
          
          <div class="form-group">
            <label>Teléfono</label>
            <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-phone"></i>
              </div>
              <input type="text" class="form-control phone-edit" name="phone" data-inputmask="&quot;mask&quot;: &quot;(999) 999-9999&quot;" data-mask="">
            </div>
          </div>
          <button class="btn btn-block btn-danger btn-flat" type="submit">Guardar</button>
          <button class="btn btn-block btn-danger btn-flat submit_button" type="button" id="cancel_edit">Cancelar</button>
        </form> 
      </div>
    </div>
  </div>
</div>

<!-- /.col (left) -->
  <!-- /.col (left) -->
  <div class="col-md-6 danger" >
    <div class="box box-danger">
      <div class="box-header">
        <h3 class="box-title">Clientes disponibles</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <input type="hidden" id="_url2" value="{{ url('') }}">
        <input type="hidden" id="_token" value="{{ csrf_token() }}">
        <table class="table table-bordered table-hover table-responsive table-striped display dt-responsive main-table" style="width:100%"  id="table" cellspacing="0">
          <thead>
             <tr>
              <th>N° acción</th>
              <th>Nombre</th>
              <th>Teléfono</th>
              <th>Dirección</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody class="main-tbody-table" id="table-tbody"></tbody>
        </table>
      </div>
    </div>          
  </div>
</div>

@endsection