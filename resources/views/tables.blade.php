@extends('layouts.admin')
@section('title', 'mesas')
@section('page_title', 'Listado de mesas')
@section('page_subtitle', '')
@push('scripts')
<script src="{{ asset('js/admin/tables.js') }}"></script>
@endpush
@section('breadcrumb')
@parent
<li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
<li><a href="{{ url('/tables') }}"><i class="fa fa-dashboard"></i> Mesas</a></li>
<li class="active">Listado</li>
@endsection
@section('content')
<link href="{{ asset('css/table.css') }}" rel="stylesheet" type="text/css">
<div class="row">
  <div class="col-md-6">
    <div class="box box-danger" id="box_to_add_desktop">
      <div class="box-header">
        <h3 class="box-title">Nueva mesa</h3>
      </div>
      <div class="box-body">
        <div class="form-group"> 
          <form id="newTable">
            <input type="hidden" id="_token" value="{{  csrf_token() }}">
            <input type="hidden" id="_url"   value="{{ url('tables') }}">

            <div class="form-group">
              <label for="exampleInputEmail1">Numero de la mesa</label>
              <div class="input-group">
                <input type="text" name="number" class="form-control" required>
                <span class="input-group-addon"><i class="fa fa-check"></i></span>
              </div>
            </div>
            <div class="checkbox icheck">
              <label>
                <input type="radio" class="input" name="status"  id="radio" value="Activa" checked=""> Activa&nbsp;&nbsp;
                <input type="radio" class="output" name="status" id="radio"  value="Inactiva"> Inactiva
              </label>
              <span class="missing_alert text-danger" id="radio_alert"></span>
            </div>
            <button class="btn btn-block btn-danger btn-flat submit_button" type="submit">Guardar</button>
          </form> 
        </div>
      </div>
    </div>
    <!-- voy aca-->
    
    <div class="box box-danger" id="box_to_edit_desktop" style="display: none;">
      <div class="box-header">
        <h3 class="box-title">Editar table</h3>
      </div>
      <div class="box-body">
        <div class="form-group"> 
          <form id="editTable">
            <input type="hidden" id="_token" value="{{  csrf_token() }}">
            <input type="hidden" id="_url"   value="{{ url('tables') }}">

            <div class="form-group">

              <label for="exampleInputEmail1">Numero de mesa</label>
              <div class="input-group">
                <input type="hidden" id="item_id">
                <input type="hidden" name="id" id="edit_id">
                <input type="text" name="number" class="form-control number-edit" required>
                <span class="input-group-addon"><i class="fa fa-check"></i></span>
              </div>
            </div>

             <div class="form-group">
               <label for="exampleInputEmail1">Estatus</label>
               <div class="input-group">
                 <input type="radio" name="status" class="status-active" value="Activa"> Activo
                 <input type="radio" name="status" class="status-inactive" value="Inactiva"> Inactivo
               </div>
             </div> 

             <button class="btn btn-block btn-danger btn-flat submit_button" type="submit">Guardar</button>
             <button class="btn btn-block btn-danger btn-flat" type="button" id="cancel_edit">Cancelar</button>
          </form> 
        </div>
      </div>
    </div>
  </div>

  <!-- /.col (left) -->
  <div class="col-md-6 danger" >
    <div class="box box-danger">
      <div class="box-header">
        <h3 class="box-title">Mesas disponibles</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <form id="main-form">
          <input type="hidden" id="_url" value="{{ url('') }}">
          <input type="hidden" id="_token" value="{{ csrf_token() }}">
          <table id="table" class="main-table table table-bordered table-striped display dt-responsive" style="width:100%">
            <thead>
              <tr>
                <th>Número de mesa</th>
                <th>Status</th>
                <th>Acciones</th>
              </tr>
            </thead>
            <tbody class="main-tbody-table" id="table-tbody"></tbody>
          </table>
        </form>
      </div>
    </div>            
  </div>

<div class="modal fade  modal-danger fade in" id="modal-danger " role="dialog" >
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">CONFIRMACIÓN DE OPERACIÓN</h4>
              </div>
              <div class="modal-body">
                <p>¿DESEA ELIMINAR ESTE REGISTRO?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-outline">Confirmar</button>
              </div>
            </div>
      </div>
  </div>
  
</div>

@endsection
@push('scripts')
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
  });
</script>
@endpush
