@extends('layouts.admin')
@section('title', 'Productos')
@section('page_title', 'Listado de productos')
@section('page_subtitle', '')
@push('scripts')
<script src="{{ asset('js/admin/product.js') }}"></script>
@endpush
@section('breadcrumb')
    @parent
    <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li><a href="{{ url('/products') }}"><i class="fa fa-dashboard"></i> Listado</a></li>
    <li class="active">Principal</li>
@endsection
@section('content')
<link href="{{ asset('css/product.css') }}" rel="stylesheet" type="text/css">

<div class="row">
  <div class="col-md-6">
    <div class="box box-danger" id="box_to_add_product">
      <div class="box-header">
        <h3 class="box-title">Nuevo productos</h3>
      </div>
      <div class="box-body">
        <div class="form-group"> 
           <form id="newProduct">
              <input type="hidden" id="_token" value="{{  csrf_token() }}">
              <input type="hidden" id="_url"   value="{{ url('products') }}">
              <div class="form-group">
                <label for="exampleInputEmail1">Nombre</label>
                    <div class="input-group">
                      <input type="text" name="name" maxlength="30" class="form-control" required>
                       <span class="input-group-addon"><i class="fa fa-check"></i></span>
                    </div>
              </div>
              <div class="form-group">
                 <label for="exampleInputEmail1">Precio</label>
                    <div class="input-group">
                      <input type="number" name="price" maxlength="5" step="any" class="form-control" required>
                       <span class="input-group-addon"><i class="fa fa-check"></i></span>
                    </div>
              </div>
              <div class="form-group">
                  <label>Descripción</label>
                  <textarea class="form-control" maxlength="50" name="description" rows="3" ></textarea>
              </div>
                 <button class="btn btn-block btn-danger btn-flat submit_button" type="submit" >Guardar</button>
          </form> 
        </div>
      </div>
    </div>
    <!-- voy aca-->

    <div class="box box-danger" id="box_to_edit_product" style="display: none;">
      <div class="box-header">
        <h3 class="box-title">Editar productos</h3>
      </div>
      <div class="box-body">
        <div class="form-group"> 
          <form id="editProduct">
            <input type="hidden" id="_token" value="{{  csrf_token() }}">
            <input type="hidden" id="_url"   value="{{ url('products') }}">

            <div class="form-group">

              <label for="exampleInputEmail1">Nombre</label>
                  <div class="input-group">
                    <input type="hidden" id="item_id">
                    <input type="hidden" name="id" id="edit_id">
                    <input type="text" name="name" maxlength="30" class="form-control name-edit" required>
                    <span class="input-group-addon"><i class="fa fa-check"></i></span>
                  </div>
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Precio</label>
                  <div class="input-group">
                     <input type="number" name="price"  step="any"  maxlength="10" class="form-control price-edit">
                     <span class="input-group-addon">.00</span>
                  </div>
            </div>
            <div class="form-group">
                <label>Descripción</label>
                <textarea class="form-control description-edit" maxlength="30" name="description" rows="3" ></textarea>
            </div>
            <button class="btn btn-block btn-danger btn-flat submit_button" type="submit">Guardar</button>
            <button class="btn btn-block btn-danger btn-flat" type="button" id="cancel_edit">Cancelar</button>
          </form> 
        </div>
      </div>
    </div>
  </div>

  <!-- /.col (left) -->
  <div class="col-md-6 danger" >
    <div class="box box-danger">
      <div class="box-header">
        <h3 class="box-title">Productos disponibles</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <input type="hidden" id="_url2" value="{{ url('') }}">
        <input type="hidden" id="_token" value="{{ csrf_token() }}">
        <table class="table table-bordered table-hover table-responsive table-striped display dt-responsive main-table" style="width:100%"  id="table" cellspacing="0">
          <thead>
             <tr>
              <th>Nombre</th>
              <th>Precio</th>
              <th>Descripción(s)</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody class="main-tbody-table" id="table-tbody"></tbody>
        </table>
      </div>
    </div>          
  </div>
</div>

@endsection