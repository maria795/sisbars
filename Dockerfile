FROM chilio/laravel-dusk-ci:php-7.2
WORKDIR /app
COPY app.entrypoint /app
COPY . .
RUN composer install
ENTRYPOINT /app/app.entrypoint

EXPOSE 8000
