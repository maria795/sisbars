<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Auth;

use App\Models\User;
use App\Models\Action;
use App\Models\Product\Category;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       Schema::defaultStringLength(191);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /* ... */

        if ($this->app->environment('production')) {
        // $this->app->register('');
        } else {
            $this->app->register('Vinkla\Hashids\HashidsServiceProvider');
        }
    }
}
