<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
  protected $table = "services";

  //Accesors
  public function getCreatedAtAttribute(){
    return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('d-m-Y h:i:s');
  }

  public function getUpdatedAtAttribute(){
    return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['updated_at'])->format('d-m-Y h:i:s');
  }

  //Relationship
  public function WorkOrder()
  {
    return $this->belongsTo('App\Models\WorkOrder');
  }

  public function Product()
  { 
    return $this->belongsTo(Product::class);
  }

}
