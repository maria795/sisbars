<?php

namespace App\Models;

use App\Models\Service;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";


    public function services()
    {
        return $this->hasMany(Service::class);
    }
}
