<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Waiter extends Model
{
    protected $table = "waiters";
    protected $fillable = ['name', 'percentage', "sales"];

    public function WorkOrder()
    {
        return $this->hasMany('App\Models\WorkOrder');
    }

}
