<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class WorkOrder extends Model
{
  protected $table = "works_orders";


  //Mutators
  public function getCreatedAtAttribute(){
  	return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('d-m-Y h:i:s');
  }

  public function getUpdatedAtAttribute(){
  	return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['updated_at'])->format('d-m-Y h:i:s');
  }

	public function Customer()
	{
	    return $this->belongsTo('App\Models\Customer');
	}

	public function Waiter()
	{
	    return $this->belongsTo('App\Models\Waiter');
	}

	public function Table()
	{
	    return $this->belongsTo('App\Models\Table');
	}

	public function Service()
	{
	    return $this->hasMany('App\Models\Service', 'work_order_id');
	}

}
