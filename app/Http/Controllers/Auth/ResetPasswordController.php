<?php
 
namespace App\Http\Controllers\Auth;
 
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
 
class ResetPasswordController extends Controller
{
    use ResetsPasswords;

    public function reset(Request $request)
    {
        $this->validate($request, $this->rules(), $this->validationErrorMessages());
 
        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password, $status="1") {;
                $status="1";
                $this->resetPassword($user, $password, $status);
            }
        );
 
        return $response == \Password::PASSWORD_RESET
              ? response()->json(['success' => "true",], 200)
              : response()->json(['success' => "false"], 422);
    }
 
    protected function resetPassword($user, $password, $status)
    {
        $user->forceFill([
            'password' => bcrypt($password),
            'status' => $status,
            'remember_token' => str_random(60),
        ])->save();

    }
}