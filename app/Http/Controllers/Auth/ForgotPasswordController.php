<?php
 
namespace App\Http\Controllers\Auth;
 
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
 
class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;
 
    public function sendResetLinkEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);
 
        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );
  
        switch ($response) {
            case \Password::INVALID_USER:
            return response()->json(['response' => "Usuario invalido",], 442);

                break;
            case \Password::INVALID_TOKEN:
            return response()->json(['response' => "Token invalido",], 442);
 
            break;
            default: 
           
            return response()->json(['response' => "Enviado con exito",], 200);
        }
    }
}