<?php

namespace App\Http\Controllers;
use App\Models\Action;
use Illuminate\Http\Request;

class ActionController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:view_logins')->only('index');
    }
    public function index()
    {
        return view('admin.action.index');    
    }
    public function indexGetActions()
    {
        $action = Action::all();
        return json_encode($action);
    }
    public function create()
    {
        return view('admin.action.create');
    }

    public function store(Request $request)
    {
        $action = Action::create($request->all());
        return json_encode(['success' => true]);
    }

    public function destroy($id)
    {
        $action = Action::find(\Hashids::decode($id)[0])->delete();
        return json_encode(['success' => true]);
    }
}
