<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class AnswerController extends Controller
{

    public function showAnswerRequestForm()
    {
        return view('auth.passwords.answer');
    }
    
    public function index(Request $request)
    {
        $answer = User::where('email', $request->email)->get();
        if($answer->count() == 0)
        {
        	return json_encode(['false' => false, 'answer' => $answer]);
        }
        else
        {
        	 return json_encode(['true' => true, 'answer' => $answer]);
        }
	}

	public function verificationAnswer(Request $request)
	{
       $answer = Hash::check($request->answer0, $request->answer1);
       
       if ($answer == true) 
       {
       		return json_encode(['success' => true]);
       }
       else
       {
       		return json_encode(['success' => false]);
       }
	}

}
