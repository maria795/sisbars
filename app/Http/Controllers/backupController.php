<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Backup;
use Carbon\Carbon;
use Artisan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Config;
use Symfony\Component\Console\Output\BufferedOutput;

class backupController extends Controller
{
    public function index()
    {
        return view('backup');
    }

    private function findDBs(string $string)
    {
        $db_list = str_replace(array('', '|', '+', "\n", "\""), '', $string);
        preg_match_all("/\ ?([A-Za-z]+)+-([0-9\-\_]+)\.sql/", $string, $matches);
        return $matches;
    }

    public function indexGet()
    {
        $output = new BufferedOutput;
        Artisan::call("snapshot:list", array(), $output);
        $db_list = $output->fetch();

        $matches = $this->findDBs($db_list);

        $matches_name = $matches[1];
        $matches_created_at = $matches[2];

        $response = array();
        foreach ($matches_name as $key => $value) {
        $response[$key] = ['id' => $key, 'name' => $matches_name[$key], 'created_at' => $matches_created_at[$key]];
        }

        return json_encode($response);
    }

  	public function store(Request $request)
    {   
        $backup = $this->createBackup($request->name);
        if (!$backup){
          json_encode(['success' => false]);
        }
        return json_encode(['success' => true, 'backup'=> $backup]);
    }

    private function createBackup($name = ""){
        $backup_datetime = date('Y-m-d_H-i-s');
        $backup_name = "{$name}-{$backup_datetime}.sql";
        try {
          Artisan::call("snapshot:create", ["name"=>$backup_name]);
        } catch (exception $e) {
          return false;
        }

        $matches = $this->findDBs($backup_name);
        $matches_name = $matches[1];
        $matches_created_at = $matches[2];

        $backup = array('id' => 9999999999, 'name' => $matches_name[0], 'created_at' => $matches_created_at[0]);

        return $backup;
    }

    public function show(Request $request, $name){
        $this->createBackup("automatically");

        $name = trim($name);
        try {
          Artisan::call("snapshot:load", ["name"=>"{$name}.sql"]);
          return json_encode(['success' => true]);
        } catch (exception $e) {
          return json_encode(['success' => false]);
        }
    }

    public function destroy($name)
    {
        $name = trim($name);
        try {
          Artisan::call("snapshot:delete", ["name"=>"{$name}.sql"]);
        } catch (exception $e) {
          return json_encode(['success' => false]);
        }
        return json_encode(['success' => true]);
    }
}
