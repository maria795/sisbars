<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Login;

class LoginController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:view_logins')->only('index');
    }

    public function index(Request $request)
    {
        return view('admin.login.index');
    }

    public function indexGet(Request $request)
    {
        $logins = Login::WithUser()->get();
         return response()->json(
        collect([
            'logins' => $logins,
        ])->toJson()
    );

    }

    public function indexGetLogins($id)
    {
        $logins = Login::WithUser()->where('user_id', $id)->orderBy('login_at', 'desc')->get();

        return response()->json(
        collect([
            'logins' => $logins,
        ])->toJson());

    }

    public function show($id){
        return view('admin.login.show', ['id' => $id]);

    }
 

}


