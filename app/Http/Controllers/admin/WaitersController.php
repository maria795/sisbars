<?php

namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Waiter;
use Carbon\Carbon;
use App\Http\Requests\StoreWaiter;

class WaitersController extends Controller
{
    public function index()
    {
        return view('waiters');
    }

    public function indexGet()
    {
        $waiter = Waiter::all()->where("sales","0");
        return json_encode($waiter);
    }

    public function store(StoreWaiter $request)
    {
        $waiter = new Waiter;
        $waiter->name = $request->name;
        $waiter->percentage = $request->percentage;
        $waiter->created_at = Carbon::now();
        $waiter->updated_at = Carbon::now();
        $waiter->save();
        return json_encode(['success' => true, 'waiter'=>$waiter]);
    }

    public function edit($id)
    {
        $waiter = Waiter::find($id);
        return json_encode(['success' => true, 'waiter' => $waiter]);
    }

    public function update(Request $request, $id)
    {
        $waiter = Waiter::find($id);
        $waiter->name = $request->name;
        $waiter->percentage = $request->percentage;
        $waiter->save();
        return json_encode(['success' => true, 'waiter' => $waiter]);
    }

    public function destroy($id)
    {
        $waiter = Waiter::find($id)->delete();
        return json_encode(['success' => true]);
    }

    public function show($id){
        $waiter = Waiter::find($id);
        return json_encode(['success' => true, 'waiter' => $waiter]);
    }
}
