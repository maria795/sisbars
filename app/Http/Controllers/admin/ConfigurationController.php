<?php
namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Configuration;
use App\Models\Customer;
use App\Models\Product;
use App\Models\Service;
use App\Models\WorkOrder;
use App\Models\Waiter;


class ConfigurationController extends Controller
{
    public function index()
    {
    	 return view('configuration');
    }

    public function update(Request $request, $id)
    {
        $configuration = Configuration::firstOrNew(array('id' => 1));
        $configuration->name = $request->name;
        $configuration->description = $request->description;
        $configuration->iva = $request->iva;
        $configuration->status = $request->status;
        $configuration->save();
        return json_encode(['success' => true, 'configuration' => $configuration]);
    }

    public function show($id = 1)
    {
        $configuration = Configuration::find($id);
        return json_encode(['success' => true, 'configuration' => $configuration]);
    }


    public function getTableDataFacture($id)
    {
        $work_order = WorkOrder::find($id);
        if ( count($work_order->service) > 0 ){
            $work_order->status = "close";
        }
        $work_order->save();
        
        $WorkOrder = WorkOrder::with('Customer','Table', 'Waiter','service.product')->where('id', $id)->get();
        $WorkOrder = $WorkOrder->filter(function ($work_order, $key){
            if(empty($work_order->service->toArray())){
                return false;
            }
            return $work_order;
        });
        return json_encode($WorkOrder);
    }

}
?>