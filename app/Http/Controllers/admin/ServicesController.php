<?php
namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\Requests;
use App\Models\Product;
use App\Models\Service;
use App\Models\WorkOrder;
use Carbon\Carbon;
use Illuminate\Http\Request;


class ServicesController extends Controller
{
    public function index()
    {
        // return view('example');
    }

    public function indexGet()
    {
        //$service = Service::all();
        //return json_encode($service);
    }

    public function Store(Request $request)
    {
        $product = Product::where('id', $request->product_id)->first();
        $work_order = WorkOrder::with('Customer','Table', 'Waiter', 'Service')->where('id', $request->work_order_id)->first();

        if ($product->count() &&  !empty($work_order)) {

            $service = new Service;
            $service->quantity = $request->quantity;

            $update = false;
            foreach ($work_order->Service as $key => $serv) {
                if ($serv->product_id == $request->product_id){
                    $service = Service::find($serv->id);
                    $service->quantity += $request->quantity;//plus
                    $update = true;
                }
            }

            $request->code = "1234";  
            $service->work_order_id = $request->work_order_id;
            // $service->code = $request->code;
            // $service->description = $request->description;
            $service->product_id = $request->product_id;
            $service->description = $request->description;
            $service->waiter_percentage = $request->waiter_percentage;

            $service->actual_price = $product->price;
            $service->created_at = Carbon::now();
            $service->updated_at = Carbon::now();
            $service->save();

            $this->changetoWaitTheWorkOrder($request->work_order_id);

            return json_encode(['success' => true, 'service' => $service, 'update'=>$update]);
        }

        return json_encode(['success' => false]);
    }

    public function update(Request $request, $id)
    {
        $service = Service::find($id);
        $service->quantity = $request->quantity;
        $service->save();

        $this->changetoWaitTheWorkOrder($service->work_order_id);

        return json_encode(['success' => true, 'service'=>$service]);
    }

    function changetoWaitTheWorkOrder($work_order_id){
        // WORK ORDER TO WAIT
        $work_order = workOrder::find($work_order_id);
        $work_order->status = 'wait';
        $work_order->save();
    }


    public function destroy(Request $request, $id)
    {
        $service = service::find($id);
        $work_order = WorkOrder::find($service->work_order_id);
        $work_order->status = 'wait';
        $work_order->save();
        $service->delete();
        return json_encode(['success' => true, 'id'=>$work_order->id]);
    }
}
