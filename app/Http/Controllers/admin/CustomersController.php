<?php
namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Http\Requests\StoreCustomer;
use Carbon\Carbon;

class CustomersController extends Controller
{
    public function index()
    {
        return view('customers');
    }

    public function indexGet()
    {
        $customer = Customer::all()->where("sales","0");
        return json_encode($customer);
    }

    public function store(StoreCustomer $request)
    {
        $customer = new Customer;
        $customer->name = $request->name;
        $customer->action_number = $request->action_number;
        $customer->phone = $request->phone;
        $customer->direction = $request->direction;
        $customer->created_at = Carbon::now();
        $customer->updated_at = Carbon::now();
        $customer->save();
        return json_encode(['success' => true, 'customer' => $customer]);
    }

    public function edit($id)
    {
        $customer = Customer::find($id);
        return json_encode(['success' => true, 'customer' => $customer]);
    }


    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);
        $customer->action_number = $request->action_number;
        $customer->name = $request->name;
        $customer->phone = $request->phone;
        $customer->direction = $request->direction;
        $customer->save();
        return json_encode(['success' => true, 'customer' => $customer]);
    }


    public function codeCustomer(Request $request)
    {   
        $customer = Customer::where('action_number', '=', trim($request->action_number))->get()->toArray();
        if (empty($customer)){
            return json_encode(['success' => false]);
        }
        return json_encode(['success' => true, 'customer' => $customer[0]]);
    }

    public function nameCustomer(Request $request)
    {   
        $customer = Customer::where('name', '=', $request->name)->get()->toArray();
        if (empty($customer)){
            return json_encode(['success' => false]);
        }
        return json_encode(['success' => true, 'customer' => $customer[0]]);
    }

    public function destroy($id)
    {
        $customer = Customer::find($id)->delete();
        return json_encode(['success' => true]);
    }

    public function show($id){
        $customer = Customer::find($id);
        return json_encode(['success' => true, 'customer' => $customer]);
    }

}
