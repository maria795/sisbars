<?php
namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use App\Http\Controllers\admin\CustomersController;
use App\Http\Requests\StoreCustomer;

use App\Models\Customer;
use App\Models\Product;
use App\Models\Service;
use App\Models\Table;
use App\Models\Waiter;
use App\Models\WorkOrder;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WorksOrdersController extends Controller
{
	public function index()
	{
		return view('dashboard');
	}

	function generationCode() {
		$key = '';
		$longitud = 6;
		$pattern = '1234567890';
		$max = strlen($pattern)-1;
		for($i=0;$i < $longitud;$i++)  {
			$key .= $pattern{mt_rand(0, $max)};
		}
		return json_encode(['code'=> $key]);
	}  

    // insert a work order
	public function Store(Request $request)
	{
        //1= no real
        //0= real
		if ($request->sales == 1) 
		{
			$tables = new Table;
			$create = $tables->firstOrCreate(['id' => $request->table_id],[
				'number' => $request->table_id,
				'status' => 'Inactiva',
				'sales' => "1",
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now()]);
			$request->table_id = $create->id;

			$waiters = new Waiter;
			$creaters = $waiters->firstOrCreate(['id' => $request->waiter_id],[
				'name' => "VR",
				'percentage' => '0',
				'sales' => "1",
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now()]);
			$request->waiter_id = $creaters->id;
		}

		if (empty($request->sales)) {
			$request->sales = "0";
		}

		$table = Table::find($request->table_id);
		$table->status = 'Inactiva';
		$table->save();

		$CustomersController = new CustomersController();

		if (!empty($request->customer_id)){
			if ($this->customerIsActive($request->customer_id)){
				return json_encode(['success' => false]);
			}
		}

		$customer = (empty($request->customer_id) && $request->current_action_number == $request->action_number )? null:Customer::where('id',$request->customer_id)->get()->toArray();

		if (empty($customer)){
			$customer = new Customer;
			$customer->name = $request->name;
			$customer->action_number = $request->action_number;
			$customer->phone = $request->phone;
			$customer->direction = $request->direction;
			$customer->sales = $request->sales;
			$customer->created_at = Carbon::now();
			$customer->updated_at = Carbon::now();
			$customer->save();
          //////////// ID OF NEW CUSTOMER
			$request->customer_id = $customer->id;
		}

		$WorkOrder = new WorkOrder();
		$WorkOrder->code = "0";
		$WorkOrder->customer_id = $request->customer_id; 
		$WorkOrder->table_id = $request->table_id; 
		$WorkOrder->waiter_id = $request->waiter_id; 
		$WorkOrder->description = $request->description;
		$WorkOrder->status = "wait";
		$WorkOrder->sales = $request->sales;
		$WorkOrder->created_at = Carbon::now();
		$WorkOrder->updated_at = Carbon::now();
		$WorkOrder->save();

		return response()->json(['success' => true, 'work_order'=> WorkOrder::with('Customer','Table', 'Waiter','Service.Product')->where('id', $WorkOrder->id)->first()], 200);
	}

	private function customerIsActive($customer_id){
		$customer = WorkOrder::where('customer_id', $customer_id)->where('isActive', '1')->get()->toArray();
		if (empty($customer)){
			return false;
		}
		return true;
	}


	public function getWorkOrderStatus($id)
	{        
		$WorkOrder = WorkOrder::find($id);
		$WorkOrder->status = "close";
		$WorkOrder->save();
	}

	function moveWorkOrder(Request $request){

		$response = (array) json_decode($this->Store($request));
		if (empty($response['work_order'])){
			return json_encode(['success' => false]);
		}


		$work_order_id = $response['work_order']->id;
		if ($request->services){
			foreach ($request->services as $key => $service_id) {
				if (!empty($service_id)){
					$service = Service::find($service_id);
					$service->work_order_id = $work_order_id;
					$service->save();
				}
			}
		}

		return json_encode($response);
	}

    // Show customer data in a work order
	public function getDataCustomer($action_number)
	{
		$customer = new Customer();
		$customer = Customer::where('action_number', $action_number)->get();
		foreach ($customer as $customers) {
			return $id = $customers->id;
		}
	}

	public function getAllDataTable()
	{
		$work_order = WorkOrder::with('Customer','Table')->get();
		return $work_order;
	}

	public function getDataReport()
	{   
		$WorkOrder = WorkOrder::with('Customer','Table', 'Waiter','Service.Product')->get();

		$WorkOrder = $WorkOrder->filter(function ($work_order, $key){
			if(empty($work_order->service->toArray())){
				return false;
			}
			return $work_order;
		});

		return response()->json($WorkOrder, 200);
	}

	public function ReportDateRange(Request $request)
	{   
		// dd($request->toArray());
		$WorkOrder = WorkOrder::with('Service.Product')->get();


		$WorkOrder = $WorkOrder->filter(function ($work_order, $key){
			if(empty($work_order->service->toArray())){
				return false;
			}

			$work_order->service = $work_order->service->filter(function ($service, $key){
				if(empty($service->toArray())){
					return false;
				}
				return $service;
			});
			return $work_order;
		});
		return response()->json($WorkOrder, 200);
	}

    //-----------------------------------------------------
    //Show the data of a work order 
	public function getDataTableWorkOrder($id)
	{
		$service = Service::with('Product')->where('work_order_id', 1)->get();
		return response()->json(
			collect([
				'service' => $service,
			])->toJson()
		);
	}

	public function getTableData($id)
	{
		$WorkOrder = WorkOrder::with('Customer','Table', 'Waiter','Service.Product')->where('id', $id)->first();
		return response()->json($WorkOrder, 200);
	}


	public function getDataTableStatus($id)
	{
		$WorkOrder = new WorkOrder();
		$WorkOrder = WorkOrder::with('Customer','Table', 'Waiter')->where('status', $status)->get();
		return response()->json(
			['WorkOrder' => $WorkOrder]
		);
	}

    //-----------------------------------------------------

    //insert the data of the list of products

	public function insertService(Request $request)
	{
		$service = new Service;
		$service->work_order_id = $request->work_order_id;
		$service->code = $request->code ;
		$service->product_id = $request->product_id ;
		$service->actual_price = $request->actual_price;
		$service->waiter_percentage = $request->waiter_percentage;
		$service->description = $request->description;
		$service->quantity = $request->quantity;
		$service->created_at = CagetTableDatarbon::now();
		$service->updated_at = Carbon::now();
		$service->save();
		return json_encode(['success' => true, 'service'=>$service]);
	}


  //delete one data of the list of added products
	public function deleteProduct($id)
	{
		$service = Service::find($id)->delete();
		return json_encode(['success' => true]);
	}

    //show the data of the list of added products

	public function getDataProduct($id)
	{
		$service = new Service();
		$service = Service::with('products')->where('id', $id)->get();
		return response()->json(
			collect([
				'service' => $service,
			])->toJson()
		);
	}

    //Change table number
	public function changeTable(Request $request)
	{
		$WorkOrder = new WorkOrder();
		$WorkOrder = WorkOrder::find($id);
		$WorkOrder->table_id = $request->table_id;
		$WorkOrder->save;
		return json_encode(['success' => true]);
	}

    //Change table code
	public function changeCode(Request $request)
	{
		$WorkOrder = new WorkOrder();
		$id = $request->work_order_id;
		$WorkOrder = WorkOrder::find($id);
		$WorkOrder->code = $request->code;
		$WorkOrder->save;
		return json_encode(['success' => $WorkOrder]);
	}

	public function delete($id)
	{
		$WorkOrder = WorkOrder::find($id);
		$table_id = $WorkOrder->table_id;
		$table = Table::find($table_id);
		$table->status = 'Activa';
		$table->save();        
		return json_encode(['success' => true]);
	}

	public function update(Request $request, $id)
	{
		$work_order = WorkOrder::find($id);

		if ($request->isActive == '0'){
			$work_order->isActive = $request->isActive;
		}

		if (!empty($request->table_id)){
			$work_order->table_id = $request->table_id;
			if ($request->isActive != '0'){
				$work_order->status = 'wait';
			}
		}
		if (!empty($request->waiter_id)){
			$work_order->waiter_id = $request->waiter_id;
			$work_order->status = 'wait';
		}
		$work_order->save();


		try{
			if (!empty($request->old_table_id) && ( !empty(WorkOrder::where('table_id', $request->table_id)->get()->toArray()) || $request->old_table_id == $request->table_id ) ){
				$old_table = Table::find($request->old_table_id);
				$old_table->status = 'Activa';
				$old_table->save();

				if (!empty($request->table_id) && $request->table_id != $request->old_table_id){
					$table = Table::find($request->table_id);
					$table->status = 'Inactiva';
					$table->save();
				}
			}
			if($work_order->sales == 1) {

				$table_delete = Table::find($work_order->table_id);
				$table_delete->delete();

				$waiter_delete = Waiter::find($work_order->waiter_id);
				$waiter_delete->delete();

				$customer = Customer::find($work_order->customer_id);
				$customer->delete();

			} 

		}catch (Exception $e){
            //NOTHING FOR HERE :D
		}        
		return json_encode(['success' => true, 'work_order'=>$work_order]);
	}
}
