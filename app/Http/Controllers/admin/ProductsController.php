<?php
namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Http\Requests\StoreProduct;
use Carbon\Carbon;

class ProductsController extends Controller
{
    public function index()
    {
        return view('products');
    }

    public function indexGet()
    {
        $product = Product::all();
        return json_encode($product);
    }

    public function create()
    {
        return view('product.create');
    }

    public function store(StoreProduct $request)
    {
        if (empty($request->description))
        {
            $request->description = " ";
        } 

   		$product = new Product;
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->created_at = Carbon::now();
        $product->updated_at = Carbon::now();
        $product->save();
        return json_encode(['success' => true, 'product'=>$product]);
    }

    public function edit($id)
    {
        $product = Product::find($id);
        return json_encode(['success' => true, 'product' => $product]);
    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->save();
        return json_encode(['success' => true, 'product' => $product]);
    }

    public function show($id){
        $product = Product::find($id);
        return json_encode(['success' => true, 'product' => $product]);
    }
 

    public function destroy($id)
    {
        $product = Product::find($id)->delete();
        return json_encode(['success' => true]);
    }

}
