<?php

namespace App\Http\Controllers\admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\Requests;
use App\Http\Requests\StoreTables;
use App\Models\Table;
use App\Models\WorkOrder;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TablesController extends Controller
{
    public function index()
    {
        return view('tables');
    }

    public function indexGet()
    {
        $table = Table::all()->where("sales","0");
        return json_encode($table);
    }

    public function getActiveTables()
    {
        $tables = Table::all();
        $tables_collection = new Collection($tables);

        $tables = $tables_collection->filter(function ($item, $key) {
            $work_order = WorkOrder::where('table_id', $item['id'])->where('isActive', '1')->get()->toArray();
            if (empty($work_order)){
                return $item;
            }
        });
        return json_encode($tables);
    }


    public function store(StoreTables $request)
    {
        $table = new Table;
        $table->number = $request->number;
        $table->status = $request->status;
        $table->created_at = Carbon::now();
        $table->updated_at = Carbon::now();
        $table->save();
        return json_encode(['success' => true, 'desktop'=>$table]);
    }

    public function edit($id)
    {
        $table = Table::find($id);
        return json_encode(['success' => true, 'desktop' => $table]);
    }

    public function update(Request $request, $id)
    {
        $table = Table::find($id);
        if (!empty($request->number)){
            $table->number = $request->number;
        }
        if (!empty($request->status)){
            $table->status = $request->status;
        }

        $table->save();

        return json_encode(['success' => true, 'desktop'=>$table]);
    }

    public function destroy($id)
    {
        $table = Table::find($id)->delete();
        return json_encode(['success' => true]);
    }

     public function show($id){
        $table = Table::find($id);
        return json_encode(['success' => true, 'desktop' => $table]);
    }

}
