<?php

namespace App\Listeners;

use App\Events\ActionUser;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;


class DatasActionsUser
{
    public $event;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ActionUser  $event
     * @return void
     */
    public function handle(ActionUser $event)
    {
        dd($event->Presentation);
    }
}
