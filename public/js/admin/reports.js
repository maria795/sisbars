var idioma={
  "sProcessing":"Procesando...",
  "sZeroRecords":"No se encontraron resultados",
  "sEmptyTable":"Ningún dato disponible en esta tabla",
  "sInfo": "",
  "lengthMenu":"Mostrar _MENU_ registros por página",
  "infoFiltered":"(Filtrado de _MAX_ total entradas)",
  "sInfoEmpty":"Mostrando registros del 0 al 0 de un total de 0 registros",
  "sSearch":"Buscar: ",
  "paginate":{
    "first":"Primero",
    "last":"Ultimo",
    "next":"Siguiente",
    "previous":"Anterior"
  },
};

$("body").on("keyup","#fini1, #ffin1",function(e){
    var key = e.keyCode;
    
    if($(this).val().length==2 || $(this).val().length==5)
    {
      if(key==8)
      {
        var vala = $(this).val().substring(0,$(this).val().length - 1);
        $(this).val(vala);
      }
      else
      {
      var vala = $(this).val();
      vala=vala+'-';
      $(this).val(vala);
      }
    }
});


function createDataTable(){

  oTable = $('.main-table').DataTable({
    "paging": true,
    "lengthChange": true,
    "fixedHeader": true,
    "searching":true,
    "language": idioma,
    "responsive":true,
    "dom": 'Bfrtip',
    "destroy": true,
    "order": [[ 0, "desc" ]],
    buttons: [
    {
      extend: 'excelHtml5',
      title: 'Reporte de datos',
      className: 'btn',
      text: "Excel",
      exportOptions: {
        columns: [':visible :not(:last-child)']
      },
    },
    {
      extend: 'csvHtml5',
      title: 'Reporte de datos',
      className: 'btn',
      text: "Csv",
      exportOptions: {
        columns: [':visible :not(:last-child)']
      },
    },
    {
      extend: 'pdfHtml5',
      title: 'Reporte de datos',
      orientation: 'landscape',
      className: 'btn',
      exportOptions: {
        columns: [':visible :not(:last-child)']
      },
      text: "Pdf",
      customize:function(doc) {
        doc.styles.tableHeader = {
          background_color:'#DF0101',
          color:'black'
        }
        doc.styles.tableBodyEven = {
           alignment: 'left'
        }
        doc.styles.tableBodyOdd = {
             alignment: 'left'
        }
      }
    },
    {
       extend: 'copy',
       title: 'Reporte de datos',
       className: 'btn',
       text: "Copiar",
       exportOptions: {
        columns: [':visible :not(:last-child)']
       },
    }
    ]
  });

    $('#fini1').keyup( function() { 
      $('#table-tbody').dataTable.ext.search.push( function( oSettings, aData, iDataIndex ) 
      {
        var iFini = document.getElementById('fini1').value;
        var iFfin = document.getElementById('ffin1').value;
        var iStartDateCol = 0;
        var iEndDateCol = 0;
        iFini=iFini.substring(6,10) + iFini.substring(3,5)+ iFini.substring(0,2);
        iFfin=iFfin.substring(6,10) + iFfin.substring(3,5)+ iFfin.substring(0,2);

        var datofini=aData[iStartDateCol].substring(6,10) + aData[iStartDateCol].substring(3,5)+ aData[iStartDateCol].substring(0,2);
        var datoffin=aData[iEndDateCol].substring(6,10) + aData[iEndDateCol].substring(3,5)+ aData[iEndDateCol].substring(0,2);

        if ( iFini === "" && iFfin === "" ){return true;}
        else if ( iFini <= datofini && iFfin === ""){return true;}
        else if ( iFfin >= datoffin && iFini === ""){return true;}
        else if (iFini <= datofini && iFfin >= datoffin){return true;}
        return false;
      });
      oTable.draw();

      $('#table-tbody').dataTable.ext.search = [];
    });

    $('#ffin1').keyup( function() { 
      $('#table-tbody').dataTable.ext.search.push( function( oSettings, aData, iDataIndex )  
      {
        var iFini = document.getElementById('fini1').value;
        var iFfin = document.getElementById('ffin1').value;
        var iStartDateCol = 0;
        var iEndDateCol = 0;

        iFini=iFini.substring(6,10) + iFini.substring(3,5)+ iFini.substring(0,2);
        iFfin=iFfin.substring(6,10) + iFfin.substring(3,5)+ iFfin.substring(0,2);

        var datofini=aData[iStartDateCol].substring(6,10) + aData[iStartDateCol].substring(3,5)+ aData[iStartDateCol].substring(0,2);
        var datoffin=aData[iEndDateCol].substring(6,10) + aData[iEndDateCol].substring(3,5)+ aData[iEndDateCol].substring(0,2);

        if ( iFini === "" && iFfin === "" ){return true;}
        else if ( iFini <= datofini && iFfin === ""){return true;}
        else if ( iFfin >= datoffin && iFini === ""){return true;}
        else if (iFini <= datofini && iFfin >= datoffin){return true;}
        return false;
      });      
      oTable.draw();

      $('#table-tbody').dataTable.ext.search = [];
    });

    oTable.columns().every(function (){
      var self = this;
        $( 'input', this.footer() ).on('keyup change',function () {
          if (self.search() !== this.value ){
            self.search( this.value ).draw();
          }
        });
    });
}

    function deleteWork(id)
    {
        var url = $("#_url1").val()+"/deleteWorkOrder/"+id;
        var item = $("#work_order_"+id);
        $.ajax({
          url:url,
          type:'DELETE',
          headers: {'X-CSRF-TOKEN': $('#workOrder #_token').val()},
          cache: false,
          success: function(response)
          {
              var json = $.parseJSON(response);
              if(json.success){
               toastr.success('Reporte Elminado');
               $.when(
                  $( item ).animate({
                    opacity: 0.25,
                  }, 500)
                ).done(
                  function (){
                    $( item ).animate({
                      opacity: 1,
                    }, 500, function (){
                    $(item).remove();
                  })
                });
              }
          },
        });
    }

$(document).ready(function(){
  $('#table-tbody').empty();
  var cars = [];
  $.get($("#_url").val(), function (data) 
  {
    var array = []
    var cars;
    var work_order_id_service = 0;
    var result = 0;
    var work_order_id = 0;
    $.each(data,function(key, work_order) 
    {
      var  div_data = '';
      var waiter_percentage = 0;
      var work_order_row = '';
      var inputs = $("<div style='display:none'></div>");
      var percentage_pay = 0;
      work_order_id = work_order.id;
      var description = work_order.description || '';
      var service_quantity = 0;
      var product_quantity = 0;
      var price = 0;
      var total = 0;
      var sum = 0;
      var group;
      var date = work_order.created_at;
      var status = work_order.status;
      if(status=='open') 
      {
        status = '<span id="unique" class="pull-right-container"><small class="label pull-right bg-green">ABIERTA</small></span>';
      }
      if(status=='wait') 
      {
        status = '<span  class="pull-right-container"><small class="label pull-right bg-yellow">POR PAGAR</small></span>';
      }
      if(status == "close") 
      {
        status = '<span  class="pull-right-container"><small class="label pull-right bg-red">CERRADO</small></span>';
      }
      var customer_id = work_order.customer_id;
      var waiter_id = work_order.waiter_id;
      var table_id = work_order.table_id;

      //CUSTOMER HERE
      var customer_name   = '';
      var customer_phone  = '';
      var customer_action_number = '';
      if (work_order.customer){
        customer_action_number =  work_order.customer.action_number;
        customer_name   =   work_order.customer.name;
        customer_phone  =  work_order.customer.phone;
      }

      var waiter_name   = '';
      var table_number  = '';
      if (work_order.waiter){
        waiter_name = work_order.waiter.name;
      }
      
      if (work_order.table){
        table_number = work_order.table.number;
      }

      $.each(work_order.service,function(k, service) 
      {
        var work_order_id_service = service.work_order_id;          
        var product_id = service.product_id;
        var product_name = '';
        var product_price = 0;
        var product_description = '';

        if (service.product){
          product_name = service.product.name;
          product_price = service.product.price;
          product_description = service.product.description;
      }


        var quantity = service.quantity;              
        var actual_price = service.actual_price;   
        waiter_percentage = service.waiter_percentage;
            //_______________agrupar producto por id para total__________________________
            var total = actual_price * quantity;  
            total =  parseFloat(total).toFixed(2);
            cars = {work_order_id_service:work_order_id_service, total:total};
            array.push(cars);
            
            groupBy = function (miarray, prop) {
              return miarray.reduce(function(groups, item) {
                var val = item[prop];
                groups[val] = groups[val] || {work_order_id_service: item.work_order_id_service, total: 0};
                groups[val].total += eval(item.total);
                return groups;
              }, {});
            }
            //______________________________________________________

            var products = "<div class='products'></div>";
            var input_name = "<input type='hidden' class='name'>";
            var input_quantity = "<input type='hidden' class='quantity' >";
            var input_price = "<input type='hidden' class='price'>";

            products = $(products);
            products.append( $(input_name).val(product_name) );
            products.append( $(input_quantity).val(quantity) );
            products.append( $(input_price).val(actual_price) );
            inputs.append(products);
          });

          if(work_order.sales == 1)
          {
            customer_name = "VR";
            table_number = "VR";
            waiter_name  = "VR"; 
          }

          work_order_row  = '<tr  id="work_order_'+work_order_id+'">';
          work_order_row +=  '<td class="t-trigger item_number date"><input type="hidden" class="work_order_ids" value='+work_order_id+'>'+date+'</td>';
          work_order_row +=  '<td data-label="status" class="status">' +status+'</td>';
          work_order_row +=  '<td data-label="status" class="table_id">' +table_number+'</td>';
          work_order_row +=  '<td data-label="status" class="waiter">' +waiter_name+'</td>';
          work_order_row +=  '<td class="t-trigger item_number code">'+description+'</td>';
          work_order_row +=  '<td data-label="status" class="total'+work_order_id+'"> </td>';
          work_order_row +=  '<td data-label="status" class="percentage_total_'+work_order_id+'"><input style="width:100%;" type="hidden" value="'+waiter_percentage+'" class="percentage_pay_'+work_order_id+'">'+waiter_percentage+'</td>';
          work_order_row +=  '<td data-label="acciones" > <div class="btn-group actions"><button type="button" data-target="#modal-danger"  data-toggle="modal" class="btn btn-danger btn-flat delete-item" title="Elimnar Mesero"><span class="glyphicon glyphicon-remove"> </span></button>  </div></td>';
          work_order_row += '</tr>';
          work_order_row = $(work_order_row).append(inputs);


          work_order_row = $(work_order_row);
          work_order_row.find('.delete-item').data('item_id', work_order_id);
          work_order_row.find('.delete-item').click(function ()
            {      
              var work_order_id = $(this).data('item_id');
              $('.done').click(function(){              
                deleteWork(work_order_id);
                $('.done').off("click");
            });
          })


      work_order_row.on("click", function() 
      {
        var work = $(this).find('.date').each(function (){
        var id = $(this).find('.work_order_ids').val();

                 $("#facture_id" ).val('');
                 $('#facture_id').val(id);
        });

        $('#facture_id').click(function (){
          var work_order_id = $('#facture_id').val();
          showInvoice(work_order_id);
        });
        $.ajax({
          url: $('#_url1').val()+"/configuration/" + 1,
          type: 'GET',
          headers: {'X-CSRF-TOKEN': $('#workOrder #_token').val()},
          cache: false,
          success: function(response)
          {
            var json = $.parseJSON(response);   
            var  fecha = new Date();
            var date  = ""+fecha.getDate()+"-"+(fecha.getMonth()+1)+"-"+fecha.getFullYear();
            var time = fecha.getHours()+":"+fecha.getMinutes();
            $('#edit_id').val(json.configuration.id);          
                  $('.company_datetime').text(date +" "+time); //listo
                  $('.company_description').text(json.configuration.description); 
                  $('.company_name').text(json.configuration.name); 
                  $('.company_iva').text(json.configuration.iva); 
                  $('.company_status').text(json.configuration.status); 
                },
              });
        $("#additional_product" ).empty();

        $('.date-sale').text(date);
        $('.customer-sale').text(customer_name);
        $('.waiter-sale').text(waiter_name);
        $('.percentage').val(waiter_percentage);

        var products = $(this).find('.products').each(function (){
          var name = $(this).find('.name').val();
          var price  = $(this).find('.price').val();
          var quantity = $(this).find('.quantity').val();    
          div_data ='<center><div class="row"> <div class="col-xs-4 col-centered" style= "word-wrap: break-word;"><span class="data-1">'+quantity+'</span></div> <div class="col-xs-4"><span class="data-2">'+name+'</span> </div> <div class="col-xs-4"><span class="data-3">'+price+'</span> </div></div></center>';       
          $('#additional_product').append(div_data);
        });


      });
      $('.main-tbody-table').append(work_order_row);
    });

    $('.foot').each( function () {
      var title = $(this).text();
      $(this).html( '<input style="width:100%;" type="text" placeholder=" '+title+'" />' );
    });

        //________________UBICAR EL TOTAL EN LA FILA QUE CORRESPONDE______________________
        result =   groupBy(array,'work_order_id_service');
        $.each(result,function(y, x) 
        {
          var total = parseFloat(x.total).toFixed(2);
          $(".total"+y).text(total);
          var percentage = $(".percentage_pay_"+y).val();             
          var total_pay = $(".total"+y).text(); 
          
          var toPay = total_pay * percentage / 100;
          parseFloat(toPay).toFixed(2);
          $(".percentage_total_"+y).text(toPay);             
        });
          //______________________________________
          createDataTable()
        });

      $(".done").click(function(){

       // number_work = 
       // url = $("#_url1").val() + "/deleteWorkOrder" + number_work



      })

});
