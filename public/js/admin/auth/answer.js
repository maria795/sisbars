$(document).ready(function(){


  $('#main-form1').submit(function(e){
    e.preventDefault();
    var email = $('#main-form2 #email').val('mariajsilvat@gmail.com');
    $('.missing_alert').css('display', 'none');

    if (!$('#main-form1 #email').val().match(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/)) {
        $('#main-form1 #email_alert').text('Correo electrónico inválido').show();
        $('#main-form1 #email').focus();
        return false;
    }
      var datas = $('#email').val();
      var route = $('#main-form1 #_url').val();
      $.ajax({
          url: route,
          type: 'GET',
          cache: false,
          dataType:'json',
          data: {email : datas},
          success : function (response){
          if(response.false)
          {
            alert("error");
          }
          else if(response.true)
          {
              e.preventDefault();
              var question = response.answer[0]['question'];

              $('.email').css("display", "none");            
              $('#question').slideDown(300);
              $('.question-1').html(question);
               //---------------------
              $('#main-form2').submit(function(e){
              e.preventDefault();

              var answer0 = $('#answer').val(); 
              var answer1 = response.answer[0]['answer'];
              var email = response.answer[0]['email'];
              var url  = $('#_redirect').val();

                    var route = url + "/verification";
                    $.ajax({
                        url: route,
                        type: 'GET',
                        cache: false,
                        dataType:'json',
                        data: {answer1 : answer1, answer0 : answer0},
                        success : function (response){
                        if(response.success == true)
                        {
                            $('#main-form2 input, #main-form2 button').attr('disabled','true');
                            
                            var route = $('#main-form2 #_url').val();
                            $.ajax({
                                url: route,
                                type: 'POST',
                                headers: {'X-CSRF-TOKEN': $('#main-form2 #_token').val()},
                                cache: false,
                                dataType:'json',
                                data: {email : email},
                                success : function (response){

                                $('#main-form2 #answer_alert').text(' Correo enviado exitosamente').show();

                                  window.setTimeout(function () {
                                     location.href = $('#main-form2 #_redirect').val();
                                   }, 2000);

                                }

                            });
                        //-------------------

                        }
                        else if(response.success == false) 
                        {
                            $('#main-form2 #answer_alert').text('Respuesta incorrecta').show();
                            $('#main-form2 #answer').focus();          
                        }

                        }
                    });
                });
               //-------------
          }
          else
          {
              $('#main-form1 #email_alert').text('Correo electrónico no encontrado').show();
              $('#main-form1 #email').focus();
          }

          }
      });
  });

});
