var oTable = null;

var idioma={
  "sProcessing":"Procesando...",
  "sZeroRecords":"No se encontraron resultados",
  "sEmptyTable":"Ningún dato disponible en esta tabla",
  "sInfo": "",
  "lengthMenu":"Mostrar _MENU_ registros por página",
  "infoFiltered":"(Filtrado de _MAX_ total entradas)",
  "sInfoEmpty":"Mostrando registros del 0 al 0 de un total de 0 registros",
  "sSearch":"Buscar: ",
  "paginate":{
    "first":"Primero",
    "last":"Ultimo",
    "next":"Siguiente",
    "previous":"Anterior"
  },
};
  var url = $('#_url1').val(); 
  var getProducts_path = url + "/getProducts"; 

  var options = {
      url: getProducts_path,
      getValue: "name",
      list: {
          match: {
              enabled: true
          },
      },
  };
  $("#input_product").easyAutocomplete(options);


$("body").on("keyup","#fini, #ffin",function(e){
    var key = e.keyCode;
    
    if($(this).val().length==2 || $(this).val().length==5)
    {
      if(key==8)
      {
        var vala = $(this).val().substring(0,$(this).val().length - 1);
        
        $(this).val(vala);
      }
      else
      {
      var vala = $(this).val();
      vala=vala+'-';
      $(this).val(vala);
      }
    }
});

function createDataTableRange(){
 
  oTable = $('.main-table').DataTable({
    "paging": true,
    "lengthChange": true,
    "fixedHeader": true,
    "searching":true,
    "language": idioma,
    "responsive":true,
    "dom": 'Bfrtip',
    "destroy": true,
    "order": [[ 0, "desc" ]],
    buttons: [
    {
      extend: 'excelHtml5',
      title: 'Reporte de datos',
      className: 'btn',
      text: "Excel"
    },
    {
      extend: 'csvHtml5',
      title: 'Reporte de datos',
      className: 'btn',
      text: "Csv"
    },
    {
      extend: 'pdfHtml5',
      title: 'Reporte de datos',
      orientation: 'landscape',
      className: 'btn',
      text: "Pdf",
      customize:function(doc) {
        doc.styles.tableHeader = {
          background_color:'#DF0101',
          color:'black'
        }
        doc.styles.tableBodyEven = {
           alignment: 'left'
        }
        doc.styles.tableBodyOdd = {
             alignment: 'left'
        }
      }
    },
    {
       extend: 'copy',
       title: 'Reporte de datos',
       className: 'btn',
       text: "Copiar"
    }
    ]
  });

    $('#fini').keyup( function() { 
      $('#table-tbody-range').dataTable.ext.search.push( function( oSettings, aData, iDataIndex ) 
      {
        var iFini = document.getElementById('fini').value;
        var iFfin = document.getElementById('ffin').value;
        var iStartDateCol = 0;
        var iEndDateCol = 0;
        iFini=iFini.substring(6,10) + iFini.substring(3,5)+ iFini.substring(0,2);
        iFfin=iFfin.substring(6,10) + iFfin.substring(3,5)+ iFfin.substring(0,2);

        var datofini=aData[iStartDateCol].substring(6,10) + aData[iStartDateCol].substring(3,5)+ aData[iStartDateCol].substring(0,2);
        var datoffin=aData[iEndDateCol].substring(6,10) + aData[iEndDateCol].substring(3,5)+ aData[iEndDateCol].substring(0,2);

        if ( iFini === "" && iFfin === "" ){return true;}
        else if ( iFini <= datofini && iFfin === ""){return true;}
        else if ( iFfin >= datoffin && iFini === ""){return true;}
        else if (iFini <= datofini && iFfin >= datoffin){return true;}
        return false;
      });
      oTable.draw();

      $('#table-tbody-range').dataTable.ext.search = [];
    });

    $('#ffin').keyup( function() { 
      $('#table-tbody-range').dataTable.ext.search.push( function( oSettings, aData, iDataIndex )  
      {
        var iFini = document.getElementById('fini').value;
        var iFfin = document.getElementById('ffin').value;
        var iStartDateCol = 0;
        var iEndDateCol = 0;

        iFini=iFini.substring(6,10) + iFini.substring(3,5)+ iFini.substring(0,2);
        iFfin=iFfin.substring(6,10) + iFfin.substring(3,5)+ iFfin.substring(0,2);

        var datofini=aData[iStartDateCol].substring(6,10) + aData[iStartDateCol].substring(3,5)+ aData[iStartDateCol].substring(0,2);
        var datoffin=aData[iEndDateCol].substring(6,10) + aData[iEndDateCol].substring(3,5)+ aData[iEndDateCol].substring(0,2);

        if ( iFini === "" && iFfin === "" ){return true;}
        else if ( iFini <= datofini && iFfin === ""){return true;}
        else if ( iFfin >= datoffin && iFini === ""){return true;}
        else if (iFini <= datofini && iFfin >= datoffin){return true;}
        return false;
      });      
      oTable.draw();

      $('#table-tbody-range').dataTable.ext.search = [];
    });
}


$(document).ready(function(){
  $('#table-tbody-range').empty();
  var cars = [];
    $('.submit_button').click(function(){

    if(oTable){
      oTable.destroy();
    }

    $('.main-tbody-table-range').html('');
        
    $.get($("#_url5").val() + '?' + 'name=' + $("#input_product").val() , function (data) 
    {
      var array = []
      var cars;
      var work_order_id_service = 0;
      var result = 0;
      var work_order_id = 0;

      $.each(data,function(key, work_order) 
      {
        var  div_data = '';
        var waiter_percentage = 0;
        var work_order_row = '';
        var inputs = $("<div style='display:none'></div>");
        var percentage_pay = 0;
        work_order_id = work_order.id;
        var description = work_order.description || '';
        var service_quantity = 0;
        var product_quantity = 0;
        var price = 0;
        var total = 0;
        var sum = 0;
        var group;
        var date = work_order.created_at;
        var status = work_order.status;
        var customer_id = work_order.customer_id;
        var waiter_id = work_order.waiter_id;
        var table_id = work_order.table_id;

        $.each(work_order.service,function(k, service) 
        {
          var work_order_id_service = service.work_order_id;          
          var product_id = service.product_id;
          var product_name = '';
          var product_price = 0;
          var product_description = '';

          if (service.product){
            product_name = service.product.name;
            product_price = service.product.price;
            product_description = service.product.description;
          }

          var quantity = service.quantity;              
          var actual_price = service.actual_price;   
          waiter_percentage = service.waiter_percentage;
          var created_at = service.created_at;
          var updated_at = service.updated_at;

          var total = actual_price * quantity;
          work_order_row  = '<tr  id="work_order_'+work_order_id+'">';
          work_order_row +=  '<td class="t-trigger">'+created_at+'</td>';
          work_order_row +=  '<td data-label="status" class="table_id">' +product_name+'</td>';
          work_order_row +=  '<td data-label="status" class="waiter">' +quantity+'</td>';
          work_order_row +=  '<td class="t-trigger item_number code">'+actual_price+'</td>';
          work_order_row +=  '<td data-label="status" class="totalProduct'+work_order_id+'">'+ total +'</td>';
          work_order_row +=  '<td data-label="status"></td>';
          work_order_row += '</tr>';
          $('.main-tbody-table-range').append(work_order_row);
        });
      });
      createDataTableRange()
    });

  });
});
