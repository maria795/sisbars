$(document).ready(function(){

$("body").on("click",'.printNow',function()
{
        var css = '@page { size: portrait; }',
        head = document.head || document.getElementsByTagName('head')[0],
        style = document.createElement('style');
        style.type = 'text/css';
        style.media = 'print';
        head.appendChild(style);
        $('.logo1').show();
        window.print();
        $('.logo1').hide();

});

function createDataTable()
      {
        var oTable = $('.main-table').DataTable({
        "paging": false,
        "lengthChange": true,
        "fixedHeader": true,
        "responsive":true,
      })

        oTable.columns().every( function () {
              var that = this;
       
              $( 'input', this.footer() ).on( 'keyup change', function () {
                  if ( that.search() !== this.value ) {
                      that
                          .search( this.value )
                          .draw();
                  }
              } );
          } );

      };

  var table;
  $('#table-tbody').empty();
  var url = $('#_url').val(); 
  var path = url + "/indexGetActions"

  $.get(path, function (data) {
     $.each(JSON.parse(data),function(key, val) {
      console.log(val);
      table += '<tr class="row'+val.id+'">';
      table +=  '<td data-label="Usuario">' +val.user+'</td>';
      table +=  '<td data-label="Acción">' +val.actions+'</td>';
      table +=  '<td data-label="Modulo">' +val.models+'</td>';
      table +=  '<td data-label="Fecha">' +val.created_at+'</td>';
      table += '</tr>';
        }); 

      $('tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Buscar '+title+'" />' );
      } );

       $.when($('.main-tbody-table').append(table)).done(createDataTable());

        $(".del-btn").click(function(){
          $("#confirm-btn").attr("href", $(this).attr('id'));
        });

        $('#confirm-btn').click(function () {
            var ID = $("#confirm-btn").attr('href');
            $('#ajax-icon').removeClass('fa fa-trash').addClass('fa fa-spin fa-refresh');

            Pace.track(function () {
              $.ajax({
                url: $("#_url").val() + '/presentation/' + ID,
                headers: {'X-CSRF-TOKEN': $('#_token').val()},
                type: 'DELETE',
                success: function (response) {
                  var json = $.parseJSON(response);
                  if(json.success){
                      $("#confirm-modal").modal("hide");
                      $('#ajax-icon').removeClass('fa fa-spin fa-refresh').addClass('fa fa-trash');
                      $('.row'+ ID +'').hide();
                      toastr.success('Presentación eliminado');
                  }
                }
              });
            });
            return false;
          });
            return false;
        });
  });
