$(document).ready(function(){

$("body").on("click",'.printNow',function()
{
        var css = '@page { size: portrait; }',
        head = document.head || document.getElementsByTagName('head')[0],
        style = document.createElement('style');
        style.type = 'text/css';
        style.media = 'print';
        head.appendChild(style);
        $('.logo1').show();
        window.print();
        $('.logo1').hide();

});

function createDataTable()
      {
        var oTable = $('.main-table').DataTable({
        "paging": false,
        "lengthChange": true,
        "fixedHeader": true,
        "responsive":true,
      })

        oTable.columns().every( function () {
              var that = this;
       
              $( 'input', this.footer() ).on( 'keyup change', function () {
                  if ( that.search() !== this.value ) {
                      that
                          .search( this.value )
                          .draw();
                  }
              } );
          } );




};

  var table;
  $('#table-tbody').empty();

     var url = $("#_url").val();
    var path = url+"/indexGetLogins";

     $.get(path, function (data) {
          $.each(JSON.parse(data),function(key, val) {
            $.each(val,function(key1, val1) 
                  { 
                    console.log(val);
                    table += '<tr class="row'+val[key1].id+'">';
                    table +=  '<td data-label="usuario">' +val[key1]['user'].name+'</td>';
                    table +=  '<td class="date" data-label="inicio">' +val[key1].login_at+'</td>';
                    table +=  '<td data-label="ip">' +val[key1].ip_address+'</td>';
                    table +=  '<td data-label="client">' +val[key1].user_agent+'</td>';
                    table += '</tr>';
                 });

      $('tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Buscar '+title+'" />' );
      } );


      $.when($('.main-tbody-table').append(table)).done(createDataTable());
  });
        return false;
  });
});