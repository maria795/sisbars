$(document).ready(function(){

  var idioma={
    "sProcessing":"Procesando...",
    "sZeroRecords":"No se encontraron resultados",
    "sEmptyTable":"Ningún dato disponible en esta tabla",
    "sInfo": "",
    "lengthMenu":"Mostrar _MENU_ registros por página",
    "infoFiltered":"(Filtrado de _MAX_ total entradas)",
    "sInfoEmpty":"Mostrando registros del 0 al 0 de un total de 0 registros",
    "sSearch":"Buscar: ",
    "paginate":{
      "first":"Primero",
      "last":"Ultimo",
      "next":"Siguiente",
      "previous":"Anterior"
    },
  };

  function createDataTable(){

  oTable = $('.main-table').DataTable({
   "paging": true,
    "lengthChange": false,
    "fixedHeader": false,
    "searching":true,
    "language": idioma,
    "responsive":true,
    'scrollY':        230,
    'scrollCollapse': true,
    "responsive":true,
    "dom": 'Bfrtip',
    'info': false,
    "destroy": true,
    'autoWidth'   : false,
    "order": [[ 0, "asc" ]],
    buttons: [
    {
      extend: 'excelHtml5',
      title: 'Reporte de datos',
      className: 'btn',
      text: "Excel",
      exportOptions: {
        columns: [ 0, 1]
      },
    },
    {
      extend: 'csvHtml5',
      title: 'Reporte de datos',
      className: 'btn',
      text: "Csv",
      exportOptions: {
        columns: [ 0, 1]
      },
    },
    {
      extend: 'pdfHtml5',
      title: 'Reporte de datos',
      orientation: 'landscape',
      className: 'btn',
      exportOptions: {
        columns: [ 0, 1]
      },
      text: "Pdf",
      customize:function(doc) {
        doc.styles.tableHeader = {
          background_color:'#DF0101',
          color:'black'
        }
        doc.styles.tableBodyEven = {
           alignment: 'left'
        }
        doc.styles.tableBodyOdd = {
             alignment: 'left'
        }
      }
    },
    {
       extend: 'copy',
       title: 'Reporte de datos',
       className: 'btn',
       text: "Copiar",
       exportOptions: {
        columns: [ 0, 1]
      },
    }
    ]
  });
  };


  $('#table tbody').empty();
  function addItem(waiter){
    var table = "";
    // Le coloco el id al item, para luego seleccionar este fila y modificar sus valores si se editan
    table += '<tr id="item_'+waiter.id+'">';
    table +=  '<td class="t-trigger item_name" data-label="N° de Presentación" >'+waiter.name+'</td>';
    table +=  '<td data-label="percentage" class="item_percentage">' +waiter.percentage+'</td>';
    table +=  '<td data-label="acciones" > <div class="btn-group actions"><button type="button" class="btn btn-danger btn-flat edit-item" title="Editar Mesero"><span class="glyphicon glyphicon-pencil"></span></button> <button type="button" data-target="#modal-danger"  data-toggle="modal" class="btn btn-danger btn-flat delete-item" title="Elimnar Mesero"><span class="glyphicon glyphicon-remove"> </span></button>  </div></td>';
    table += '</tr>';

    table = $(table);
    table.find('.delete-item').data('item_id', waiter.id);
    table.find('.delete-item').click(function ()
    {      
      var waiter_id = $(this).data('item_id');
      var item = $(this).parent().parent().parent();
      $('.done').click(function(){              
          deleteWaiter(waiter_id, item);
          $('.done').off("click");
      });
    })

    table.find('.edit-item').data('item_id', waiter.id);
    table.find('.edit-item').click(function (){
      var waiter_id = $(this).data('item_id');
      var item = $(this).parent().parent().parent();
      showWaiter(waiter_id, item);
    })

    $('.main-tbody-table').append(table);

    // tableScrollBottom();
  }

  // function tableScrollBottom(){
    //Si exista el div $('.dataTables_scrollBody') este es creado cuando se muestra el scroll de la tabla
  //   if ($('.dataTables_scrollBody')[0]){
  //     $('.dataTables_scrollBody').animate({scrollTop:$('.dataTables_scrollBody')[0].scrollHeight }, 1000);
  //   }
  // }

  // createDataTable();//Ejecutar esta linea solo una vez
  $.get($('#main-form #_url').val() + "/getWaiters" , function (data) {
    $.each(JSON.parse(data),function(key, waiter) {
      addItem(waiter);
    }); 
    createDataTable();
    // tableScrollBottom();
  });


  $('#newWaiter').submit(function (e){
    e.preventDefault();
    var submit_button = $(this).find('.submit_button');
    submit_button.attr('disabled', true);

    var data = $('#newWaiter').serialize();
    $.ajax(
    {
      url: $('#newWaiter #_url').val(),
      headers: {'X-CSRF-TOKEN': $('#newWaiter #_token').val()},
      type: 'POST',
      cache: false,
      data: data,   
      success: function(response)
      {
        var json = $.parseJSON(response);
        if(json.success){
          toastr.success('Mesonero almacenado');
          $("#newWaiter")[0].reset();
          addItem(json.waiter);
        } 
      },
      error: function(data) 
      {
        var errors = data.responseJSON;
        $.each(errors.errors, function( key, value ) {
        toastr.error(value);
        return false;
        });  
      },
      complete: function (){
        submit_button.attr('disabled', false);
      }
    });  
    return false;
  });

  $('#editWaiter').submit(function (e){
    
    e.preventDefault();
   var submit_button = $(this).find('.submit_button_edit');

    submit_button.attr('disabled', true);

    var data = $('#editWaiter').serialize();
    // Recupero el id del item (en la tabla por ejemplo #item_1) para poder modificar sus valores
    var item_id = $('#item_id').val(); 
    // Selecciono la fila completa (la cual es la que tiene el id del item ejemplo #item_1)
    var item = $('#'+item_id);

    $('#item_id').val(''); 
    if (item_id){
      $.ajax(
      {
        url: $('#editWaiter #_url').val()+ '/' + $('#edit_id').val(),
        headers: {'X-CSRF-TOKEN': $('#editWaiter #_token').val()},
        type: 'PUT',
        cache: false,
        data: data,   
        success: function(response)
        {
          var json = $.parseJSON(response);
          if(json.success){
            toastr.success('Mesonero Editado');
            // Inicio una animacion de parpadeo para saber cual item se edito
            $.when(
              $( item ).animate({
                opacity: 0.25,
              }, 500)
            ).done(function (){
              $( item ).animate({
                opacity: 1,
              }, 500)
            });
            // fin del codigo de la animacion del item

            $("#editWaiter")[0].reset();

            // Modifico los valores de item que ya he seleccionado desde su item_id
            // se usa el metodo text por que no son input y se busca el modificar el texto en pantalla
            // el metodo find es muy descriptivo, pero tener presente que la variable item no es mas que la
            // fila (tr) y lo que hago es buscar por el nombre de la clase que le di cuando fueron agregados
            // a la tabla es decir cuando llame a la function addItem
            item.find(".item_name").text(json.waiter.name);
            item.find(".item_percentage").text(json.waiter.percentage);

            // Realizo la misma accion del boton cancelar
            $('#box_to_edit_waiter').hide();
            $('#box_to_add_waiter').show();
          } 
        },
        error: function(data) 
        {
          var errors = data.responseJSON;
          $.each(errors.errors, function( key, value ) {
          toastr.error(value);
          return false;
          });  
        },
        complete: function (){
          submit_button.attr('disabled', false);
        }
      });
    }
    return false;
  });


  function showWaiter(id, item){
    var url = $('#editWaiter #_url').val() + "/"+id;
    $.ajax(
    {
      url: url,
      type: 'GET',
      headers: {'X-CSRF-TOKEN': $('#newWaiter #_token').val()},
      cache: false,
      success: function(response)
      {
          $('#box_to_edit_waiter').show();
          $('#box_to_add_waiter').hide();
          var json = $.parseJSON(response); 
          $('#item_id').val(item.attr('id')); // guardo el id (table id) del item que estoy editando para luego modificarlo
          $('#edit_id').val(json.waiter.id); // id (item o waiter id) para enviarlo en el formulario
          $('.name-edit').val(json.waiter.name); //  name (item o waiter)
          $('.percentage-edit').val(json.waiter.percentage); // percentage (item o waiter)
      },
    });  
    return false;
  }

  function deleteWaiter(id, item){

    var url = $('#newWaiter #_url').val();
    url = url +"/"+ id;
    $.ajax(
    {
      url: url,
      headers: {'X-CSRF-TOKEN': $('#newWaiter #_token').val()},
      type: 'DELETE',
      cache: false,
      dataType: 'json',
      success: function(response){
        if (response.success){
          toastr.success('Mesonero Elminado');
          // Inicio una animacion de parpadeo para saber cual item se edito
          $.when(
            $( item ).animate({
              opacity: 0.25,
            }, 500)
          ).done(
            function (){
              $( item ).animate({
                opacity: 1,
              }, 500, function (){
              // Esta tercera funcion es un callback (una funcion que no se ejecuta inmediatamente sino que espera que termine)
              // un proceso asincrono

              //se remueve el item despues de la animacion de parpadeo
              $(item).remove();
            })
          });
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
                      toastr.error('No puede eliminarlo, este dato esta asociado a otra tabla');       
       }
    });
    return false;
  }


  $('#cancel_edit').click(function (){
    //Vuelvo a mostrar el formulario de agregar cliente
    $('#box_to_edit_waiter').hide();
    $('#box_to_add_waiter').show();
  });

});