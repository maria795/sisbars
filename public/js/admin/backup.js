$(document).ready(function(){

  var idioma={
    "sProcessing":"Procesando...",
    "sZeroRecords":"No se encontraron resultados",
    "sEmptyTable":"Ningún dato disponible en esta tabla",
    "sInfo": "",
    "lengthMenu":"Mostrar _MENU_ registros por página",
    "infoFiltered":"(Filtrado de _MAX_ total entradas)",
    "sInfoEmpty":"Mostrando registros del 0 al 0 de un total de 0 registros",
    "sSearch":"Buscar: ",
    "paginate":{
      "first":"Primero",
      "last":"Ultimo",
      "next":"Siguiente",
      "previous":"Anterior"
    },
  };

  function createDataTable(){

  oTable = $('.main-table').DataTable({
   "paging": true,
    "lengthChange": false,
    "fixedHeader": false,
    "searching":true,
    "language": idioma,
    "responsive":true,
    'scrollY':        230,
    'scrollCollapse': true,
    "responsive":true,
    "dom": 'Bfrtip',
    'info': false,
    "destroy": true,
    'autoWidth'   : false,
    "order": [[ 2, "desc" ]],
    buttons: [
    {
      extend: 'excelHtml5',
      title: 'Reporte de datos',
      className: 'btn',
      text: "Excel",
      exportOptions: {
        columns: [ 0, 1]
      },
    },
    {
      extend: 'csvHtml5',
      title: 'Reporte de datos',
      className: 'btn',
      text: "Csv",
      exportOptions: {
        columns: [ 0, 1]
      },
    },
    {
      extend: 'pdfHtml5',
      title: 'Reporte de datos',
      orientation: 'landscape',
      className: 'btn',
      exportOptions: {
        columns: [ 0, 1]
      },
      text: "Pdf",
      customize:function(doc) {
        doc.styles.tableHeader = {
          background_color:'#DF0101',
          color:'black'
        }
        doc.styles.tableBodyEven = {
           alignment: 'left'
        }
        doc.styles.tableBodyOdd = {
             alignment: 'left'
        }
      }
    },
    {
       extend: 'copy',
       title: 'Reporte de datos',
       className: 'btn',
       text: "Copiar",
       exportOptions: {
        columns: [ 0, 1]
      },
    }
    ]
  });
  };

  $('#table tbody').empty();
  function addItem(backup){
    var table = "";
    // Le coloco el id al item, para luego seleccionar este fila y modificar sus valores si se editan
    table += '<tr id="item_'+backup.id+'">';
    table +=  '<td class="t-trigger item_name" data-label="N° de Presentación" >'+backup.name+'</td>';
    table +=  '<td data-label="Fecha de Creación" class="create_at">' +backup.created_at+'</td>';
    table +=  '<td data-label="acciones" > <div class="btn-group actions"><button type="button" data-target="#modal-danger-backup"  data-toggle="modal" class="btn btn-danger btn-flat edit-item" title="Aplicar copia de seguridad"><span class="glyphicon glyphicon-floppy-open"></span></button>   <button type="button" data-target="#modal-danger-backup-delete"  data-toggle="modal" class="btn btn-danger btn-flat delete-item delete_backup" title="Elimnar copia de seguridad"><span class="glyphicon glyphicon-remove"> </span></button>  </div></td>';
    table += '</tr>';

    table = $(table);
    table.find('.delete-item').data('item_id', backup.id);
    table.find('.delete-item').data('name_backup', backup.name);
    table.find('.delete-item').data('date_backup', backup.created_at);

    table.find('.delete-item').click(function ()
    {  
      var backup_id = $(this).data('item_id');
      var item = $(this).parent().parent().parent();
      var backup_name = $(this).data('name_backup');
      var backup_date = $(this).data('date_backup');

      $('#modal-danger-backup-delete').on('show.bs.modal', function (e) { 
          $(".name").text(backup_name);
      });
      $('#modal-danger-backup-delete').on('hide.bs.modal', function (e) { 
          $(".name").text("");
      });

      $('.done_backup').click(function(){              
          deleteBackup(backup_id, item);
          $('.done_backup').off("click");
      });
    })

    table.find('.edit-item').data('item_id', backup.id);
    table.find('.edit-item').data('name_backup', backup.name);
    table.find('.edit-item').data('date_backup', backup.created_at);

    table.find('.edit-item').click(function (){

      var backup_id = $(this).data('item_id');
      var item = $(this).parent().parent().parent();
      var backup_name = $(this).data('name_backup');
      var backup_date = $(this).data('date_backup');

      $('#modal-danger-backup').on('show.bs.modal', function (e) { 
          $(".name").text(backup_name);
      });
      $('#modal-danger-backup').on('hide.bs.modal', function (e) { 
          $(".name").text("");
      });

      $('.done_backup').click(function(){              
       appBackud(backup_id, item);
      });
    })

    $('.main-tbody-table').append(table);

    // tableScrollBottom();
  }


  $.get($('#main-form #_url').val() + "/getBackup" , function (data) {
    $.each(JSON.parse(data),function(key, backup) {
      addItem(backup);
    }); 
    createDataTable();
    // tableScrollBottom();
  });

  $('#newbackup').submit(function (e){
    e.preventDefault();
    var submit_button = $(this).find('.submit_button');
    submit_button.attr('disabled', true);
    var data = $('#newbackup').serialize();
    $.ajax(
    {
      url: $('#newbackup #_url').val(),
      headers: {'X-CSRF-TOKEN': $('#newbackup #_token').val()},
      type: 'POST',
      cache: false,
      data: data,   
      success: function(response)
      {
        var json = $.parseJSON(response);
        if(json.success){
          toastr.success('Copia de seguridad almacenado');
          addItem(json.backup);
          $("#newbackup")[0].reset();
        } 
      },
      error: function(data) 
      {
        var errors = data.responseJSON;
        $.each(errors.errors, function( key, value ) {
        toastr.error(value);
        return false;
        });  
      },
      complete: function (){
        submit_button.attr('disabled', false);
      }
    });  
    return false;
  });

  function appBackud(id, item){
    var url = $('#newbackup #_url').val() + "/"+ item.find('.item_name').text() + '-' + $.trim(item.find('.create_at').text());
    $.ajax(
    {
      url: url,
      type: 'GET',
      headers: {'X-CSRF-TOKEN': $('#newbackup #_token').val()},
      cache: false,
      success: function(response)
      {
        var json = $.parseJSON(response);
        if(json.success){
          toastr.success('Aplicada copia de seguridad');
          // addItem(json.backup);
          window.location.reload();
        } 
      },
    });  
    return false;
  }

  function deleteBackup(id, item){
    var url = $('#newbackup #_url').val();
    url = url +"/"+ item.find('.item_name').text() + '-' + $.trim(item.find('.create_at').text());
    $.ajax(
    {
      url: url,
      headers: {'X-CSRF-TOKEN': $('#newbackup #_token').val()},
      type: 'DELETE',
      cache: false,
      dataType: 'json',
      success: function(response){
        if (response.success){
          toastr.success('Copia de seguridad, Eliminado');
          // Inicio una animacion de parpadeo para saber cual item se edito
          $.when(
            $( item ).animate({
              opacity: 0.25,
            }, 500)
          ).done(
            function (){
              $( item ).animate({
                opacity: 1,
              }, 500, function (){
              // Esta tercera funcion es un callback (una funcion que no se ejecuta inmediatamente sino que espera que termine)
              // un proceso asincrono

              //se remueve el item despues de la animacion de parpadeo
              $(item).remove();
            })
          });
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
          toastr.error('No puede eliminarlo, este dato esta asociado a otra tabla');       
      }
    });
    return false;
  }



});
