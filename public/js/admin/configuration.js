$(document).ready(function(){
      $.ajax(
      {
        url: $('#_url').val() + '/' + 1,
        type: 'GET',
        headers: {'X-CSRF-TOKEN': $('#newConfiguration #_token').val()},
        cache: false,
        success: function(response)
        {
            var json = $.parseJSON(response);    
            var  fecha = new Date();
            var date  = ""+fecha.getDate()+"-"+(fecha.getMonth()+1)+"-"+fecha.getFullYear();
            var time = fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();
            console.log(json.configuration.status);
            switch (json.configuration.status){

              case "SI":
                $(".status-active").next().trigger('click'); 
              break;
              case "NO":
                $(".status-inactive").next().trigger('click');
              break;
            }
            $('.datetime').text(date +" "+time);
            $('#edit_id').val(json.configuration.id); 
            $('.name').val(json.configuration.name); 
            $('.iva').val(json.configuration.iva);
            $('.description').val(json.configuration.description);

        },
      });  
});

    $('#editConfiguration').submit(function(){
      var data = $('#editConfiguration').serialize();
      $.ajax({
        url: $('#_url').val() + '/' + 1,
        headers: {'X-CSRF-TOKEN': $('#editConfiguration #_token').val()},
        type: 'PUT',
        cache: false,
        data: data,
          success: function (response) {
            var json = $.parseJSON(response);
            if(json.success)
             {

                toastr.success('Datos actualizados exitosamente');
             }
          },error: function (data) {
          var errors = data.responseJSON;
          $.each( errors.errors, function( key, value ) {
              toastr.error(value);
              return false;
          });
          }
        });
      return false;
    });