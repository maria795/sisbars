$(document).ready(function(){

    var idioma={
      "sProcessing":"Procesando...",
      "sZeroRecords":"No se encontraron resultados",
      "sEmptyTable":"Ningún dato disponible en esta tabla",
      "sInfo": "",
      "lengthMenu":"Mostrar _MENU_ registros por página",
      "infoFiltered":"(Filtrado de _MAX_ total entradas)",
      "sInfoEmpty":"Mostrando registros del 0 al 0 de un total de 0 registros",
      "sSearch":"Buscar: ",
      "paginate":{
        "first":"Primero",
        "last":"Ultimo",
        "next":"Siguiente",
        "previous":"Anterior"
      },
    };

    function createDataTable(){
    oTable = $('.main-table').DataTable({
      "paging": true,
      "lengthChange": false,
      "fixedHeader": false,
      "searching":true,
      "language": idioma,
      "responsive":true,
      'scrollY':        230,
      'scrollCollapse': true,
      "responsive":true,
      "dom": 'Bfrtip',
      'info': false,
      "destroy": true,
      'autoWidth'   : false,
      "order": [[ 0, "asc" ]],
      buttons: [
      {
        extend: 'excelHtml5',
        title: 'Reporte de datos',
        className: 'btn',
        text: "Excel",
        exportOptions: {
        columns: [ 0, 1,2,3]
        },
      },
      {
        extend: 'csvHtml5',
        title: 'Reporte de datos',
        className: 'btn',
        text: "Csv",
        exportOptions: {
        columns: [ 0, 1,2,3]
        },
      },
      {
        extend: 'pdfHtml5',
        title: 'Reporte de datos',
        orientation: 'landscape',
        className: 'btn',
        exportOptions: {
        columns: [ 0, 1,2,3]
        },
        text: "Pdf",
        customize:function(doc) {
          doc.styles.tableHeader = {
            background_color:'#DF0101',
            color:'black'
          }
          doc.styles.tableBodyEven = {
             alignment: 'left'
          }
          doc.styles.tableBodyOdd = {
               alignment: 'left'
          }
        }
      },
      {
         extend: 'copy',
         title: 'Reporte de datos',
         className: 'btn',
         text: "Copiar",
         exportOptions: {
          columns: [ 0, 1,2,3]
         },
      }
      ]
    });
      
    };


    $('#table tbody').empty();
    function addItem(customer){
      var table = "";
      // Le coloco el id al item, para luego seleccionar este fila y modificar sus valores si se editan
      table += '<tr id="item_'+customer.id+'">';
      table +=  '<td class="t-trigger item_action_number" data-label="" >'+customer.action_number+'</td>';
      table +=  '<td data-label="price" class="item_name">' +customer.name+'</td>';
      table +=  '<td data-label="description" class="item_phone">' +( customer.phone || '') +'</td>';
      table +=  '<td data-label="description" class="item_direction">' +( customer.direction || '')+'</td>';
      table +=  '<td data-label="acciones" > <div class="actions btn-group"><button type="button" class=" btn btn-danger btn-flat edit-item"><span class="glyphicon glyphicon-pencil" title="Editar Cliente"></span></button> <button type="button" data-target="#modal-danger"  data-toggle="modal" class="btn btn-danger btn-flat delete-item" title="Eliminar Cliente"><span class="glyphicon glyphicon-remove"> </span></button>  </div></td>';
      table += '</tr>';
      table = $(table);

      table.find('.delete-item').data('item_id', customer.id);
      table.find('.delete-item').click(function ()
      {      
        var customer_id = $(this).data('item_id');
        var item = $(this).parent().parent().parent();
        $('.done').click(function(){              
            deleteCustomer(customer_id, item);
            $('.done').off("click");
        });
      })

      table.find('.edit-item').data('item_id', customer.id);
      table.find('.edit-item').click(function (){
        var customer_id = $(this).data('item_id');
        var item = $(this).parent().parent().parent();
        showCustomer(customer_id, item);
      })

      $('.main-tbody-table').append(table);

      // tableScrollBottom();
    }

    // function tableScrollBottom(){
      //Si exista el div $('.dataTables_scrollBody') este es creado cuando se muestra el scroll de la tabla
    //   if ($('.dataTables_scrollBody')[0]){
    //     $('.dataTables_scrollBody').animate({scrollTop:$('.dataTables_scrollBody')[0].scrollHeight }, 1000);
    //   }
    // }

    // createDataTable();//Ejecutar esta linea solo una vez
  $.get($('#_url2').val() + "/getCustomers" , function (data) {
      $.each(JSON.parse(data),function(key, customer) {
        addItem(customer);
      }); 
      createDataTable();
      // tableScrollBottom();
    });


    $('#newCustomer').submit(function (e){
      e.preventDefault();
      var submit_button = $(this).find('.submit_button');
      submit_button.attr('disabled', true);

      var data = $('#newCustomer').serialize();
      $.ajax(
      {
        url: $('#newCustomer #_url').val(),
        headers: {'X-CSRF-TOKEN': $('#newCustomer #_token').val()},
        type: 'POST',
        cache: false,
        data: data,   
        success: function(response)
        {
          var json = $.parseJSON(response);
          if(json.success){
            toastr.success('Cliente almacenado');
            $("#newCustomer")[0].reset();
            addItem(json.customer);
          } 
        },
        error: function(data) 
        {
          var errors = data.responseJSON;
          $.each(errors.errors, function( key, value ) {
          toastr.error(value);
          return false;
          });  
        },
        complete: function (){
          submit_button.attr('disabled', false);
        }
      });  
      return false;
    });

    $('#editCustomer').submit(function (e){
      
      e.preventDefault();

      var data = $('#editCustomer').serialize();
      // Recupero el id del item (en la tabla por ejemplo #item_1) para poder modificar sus valores
      var item_id = $('#item_id').val(); 
      // Selecciono la fila completa (la cual es la que tiene el id del item ejemplo #item_1)
      var item = $('#'+item_id);

      /*
        Limpio el item_id para que no se pueda presionar 
        muchas veces el boton de guardar (Formulario de editar customer)
      */
      $('#item_id').val(''); 
      if (item_id){
        $.ajax(
        {
          url: $('#editCustomer #_url').val()+ '/' + $('#edit_id').val(),
          headers: {'X-CSRF-TOKEN': $('#editCustomer #_token').val()},
          type: 'PUT',
          cache: false,
          data: data, 
          success: function(response)
          {
            var json = $.parseJSON(response);
            if(json.success){
              toastr.success('Cliente Editado');
              // Inicio una animacion de parpadeo para saber cual item se edito
              $.when(
                $( item ).animate({
                  opacity: 0.25,
                }, 500)
              ).done(function (){
                $( item ).animate({
                  opacity: 1,
                }, 500)
              });
              // fin del codigo de la animacion del item

              $("#editCustomer")[0].reset();

              // Modifico los valores de item que ya he seleccionado desde su item_id
              // se usa el metodo text por que no son input y se busca el modificar el texto en pantalla
              // el metodo find es muy descriptivo, pero tener presente que la variable item no es mas que la
              // fila (tr) y lo que hago es buscar por el nombre de la clase que le di cuando fueron agregados
              // a la tabla es decir cuando llame a la function addItem
              item.find(".item_name").text(json.customer.name);
              item.find(".item_phone").text(json.customer.phone);
              item.find(".item_action_number").text(json.customer.action_number);
              item.find(".item_direction").text(json.customer.direction);

              // Realizo la misma accion del boton cancelar
              $('#box_to_edit_customer').hide();
              $('#box_to_add_customer').show();
            } 
          },
          error: function(data) 
          {
            var errors = data.responseJSON;
            $.each(errors.errors, function( key, value ) {
            toastr.error(value);
            return false;
            });  
          },
          complete: function (){
           // submit_button.attr('disabled', false);
          }
        });
      }
      return false;
    });

    function showCustomer(id, item)
    {
      var url = $('#editCustomer #_url').val() + "/"+id;
      $.ajax(
      {
        url: url,
        type: 'GET',
        headers: {'X-CSRF-TOKEN': $('#newCustomer #_token').val()},
        cache: false,
        success: function(response)
        {
            $('#box_to_edit_customer').show();
            $('#box_to_add_customer').hide();
            var json = $.parseJSON(response); 
            $('#item_id').val(item.attr('id')); // guardo el id (table id) del item que estoy editando para luego modificarlo
            $('#edit_id').val(json.customer.id); // id (item o product id) para enviarlo en el formulario
            $('.name-edit').val(json.customer.name); //  name (item o product)
            $('.phone-edit').val(json.customer.phone); // price (item o product)
            $('.direction-edit').val(json.customer.direction); // description (item o product)
            $('.action_number-edit').val(json.customer.action_number); // description (item o product)
        },
      });  
      return false;
    }

    function deleteCustomer(id, item)
    {

      var url = $('#newCustomer #_url').val();
      url = url +"/"+ id;
      $.ajax(
      {
        url: url,
        headers: {'X-CSRF-TOKEN': $('#newCustomer #_token').val()},
        type: 'DELETE',
        cache: false,
        dataType: 'json',
        success: function(response){
          if (response.success){
            toastr.success('Cliente Elminado');
            // Inicio una animacion de parpadeo para saber cual item se edito
            $.when(
              $( item ).animate({
                opacity: 0.25,
              }, 500)
            ).done(
              function (){
                $( item ).animate({
                  opacity: 1,
                }, 500, function (){
                // Esta tercera funcion es un callback (una funcion que no se ejecuta inmediatamente sino que espera que termine)
                // un proceso asincrono

                //se remueve el item despues de la animacion de parpadeo
                $(item).remove();
              })
            });
          }
        },
        error: function(response) 
        {
          console.log(response);
        }
      });
      return false;
    }


    $('#cancel_edit').click(function (){
      //Vuelvo a mostrar el formulario de agregar cliente
      $('#box_to_edit_customer').hide();
      $('#box_to_add_customer').show();
    });

});