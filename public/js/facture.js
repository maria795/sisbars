//GLOBAL IVA 
var iva = 0;

function showInvoice(work_order_id){
  setIva();

  url = $('#_url1').val() +"/facture/"+ work_order_id;
  $.get(url, function (data) {
    $.each(JSON.parse(data),function(key, work_order) {
      var  div_data = '';
      var work_order_row = '';
      var percentage_pay = 0;
      var service_quantity = 0;
      var product_quantity = 0;
      var price = 0;
      var total = 0;
      var sum = 0;
      var date = work_order.created_at;
      var status = work_order.status;
      var total_percentage;
      var total_sum ;
      var total_general ;
      var total_sum=0;
      var total_percentage=0;
      var actual_price =0;
      var iva =0;
      var action_number = 'N/A';
      var percentage_iva =0;
      var customer_name = 'N/A';
      var waiter_name = 'N/A';

      if (work_order.customer){
        customer_name = work_order.customer.name;
      }

      if (work_order.customer){
        action_number = work_order.customer.action_number;
      }
      if (work_order.waiter){
        waiter_name = work_order.waiter.name;
      }

      let sales = $('.sales').val();
      
      if (sales == 1 || work_order.sales == 1) 
      {
        $(".data").css("display","none");
      }
      else
      {
        $(".data").css("display","block");
      }

      if(work_order.sales == 1)
      {
        $('.customer-name').text("VP");
        $('.action-number').text("VP");
        $('.waiter-name').text("VP");
      }
      else
      {
        $('.customer-name').text(customer_name);
        $('.action-number').text(action_number);
        $('.waiter-name').text(waiter_name);
      }

      $('.main').html('');

      if (work_order.service.length == 0){
        toastr.error('No puede facturar, no tiene servicios');
        $('#modal-facture').modal('toggle');
        return false
      }

      $.each(work_order.service,function(k, service) {
        var id = service.id;
        var product_name = service.product.name;
        var quantity = service.quantity;              
        actual_price = service.actual_price;
        total = actual_price * quantity;  // Por producto * cant = total
        total =  parseFloat(total).toFixed(2);

        sum = eval(sum) + eval(total);

        work_order_row  =  '<tr  style="text-aling= center; border-top: 1px solid black; border-collapse: collapse;">';
        work_order_row +=  '<td class="t-trigger item_number code"> '+quantity+'</td>';
        work_order_row +=  '<td class="t-trigger item_number code"> '+product_name+" "+'</td>';
        work_order_row +=  '<td width="50" class="t-trigger item_number code" style="text-align: right;"> '+total+'</td>';
        work_order_row += '</tr>';

        $('.main').append(work_order_row);
      });

      $('.tfoot').html('')
      total_sum =  parseFloat(sum).toFixed(2);
      var percentage = $(".percentage").val();
      percentage_waiter = eval(total_sum) * eval(percentage) / 100; 

      iva = parseFloat($('.iva').val()) || 0;
      percentage_iva =  total_sum * iva / 100; 
      total_percentage =  parseFloat(percentage_iva).toFixed(2);
      totales  = eval(total_sum) + eval(total_percentage) + eval(percentage_waiter);



      work_order_foot1  = '<tr style="text-aling= center; border-top: 1px solid black; border-collapse: collapse;" class="row1">';
      work_order_foot1 += '<th colspan="2">SubTOTAL</th>';
      work_order_foot1 += '<th  class="t-trigger item_number code" style="text-align: right;">'+total_sum+'</th>';
      work_order_foot1 += '</tr>';

      if(total_percentage != 0.00) 
      {
      work_order_foot2  = '<tr  style="text-aling="center" class="row2">';
      work_order_foot2 += '<th colspan="2"> I.V.A </th>';
      work_order_foot2 += '<th  style="text-align: right;"><span>'+total_percentage+'</span></th>';
      work_order_foot2 += '</tr>';      
      }
      else
      {
      work_order_foot2  = '<tr  style="text-aling="center" class="row2">';
      work_order_foot2 += '<th colspan="2"></th>';
      work_order_foot2 += '<th></th>';
      work_order_foot2 += '</tr>';
      }

      work_order_foot3  = '<tr  style=" text-aling="center" class="row3">';
      work_order_foot3 += '<th colspan="2"> A PAGAR </th>';
      work_order_foot3 += '<th style="text-align: right;">'+totales.toFixed(2)+'</td>';
      work_order_foot3 += '</tr>';

      work_order_foot4  = '<tr  style="text-aling="center" class="row2">';
      work_order_foot4 += '<th colspan="2">SERVICIO</th>';
      work_order_foot4 += '<th style="text-align: right;">'+percentage_waiter.toFixed(2)+'</th>';
      work_order_foot4 += '</tr>';

      var w1 = work_order_foot1 + work_order_foot2 + work_order_foot4 + work_order_foot3;
      $('.tfoot').append(w1);


      var table = $('#'+work_order_id).children().first().attr('data-target', '#modal-danger-table').attr('data-toggle', 'modal');
      table.removeClass('bg-yellow').addClass('bg-red none');
      table.find('.progress-bar').css('width', '90%');
      
    }); 
  }); 
}

function setIva(){
    $.ajax({
    url: $('#_url1').val()+"/configuration/" + 1,
    type: 'GET',
    headers: {'X-CSRF-TOKEN': $('#newService #_token').val()},
    cache: false,
    success: function(response)
    {
      var json = $.parseJSON(response);   
      var fecha = new Date();
      var date  = ""+fecha.getDate()+"-"+(fecha.getMonth()+1)+"-"+fecha.getFullYear();
      var time = fecha.getHours()+":"+fecha.getMinutes()+":"+fecha.getSeconds();

      $('#edit_id').val(json.configuration.id);          
      $('.company_datetime').text(date +" "+time); //listo
      $('.company_description').text(json.configuration.description); 
      $('.company_name').text(json.configuration.name); 
      $('.company_iva').text(json.configuration.iva);
      $('.iva').val(json.configuration.iva);

      iva = json.configuration.iva;
    },
  });
}

setIva();