var waiter_percentage = 0;
var flag_for_edit_services_from_form_new_services = false;

$(document).ready(function(){

  var url = $('#_url').val(); 

  var waiters_path = url + "/getWaiters";
  var getTables_path = url + "/getActiveTables";
  var actionCustomer_path = url + "/actionCustomer"; 
  var getProducts_path = url + "/getProducts"; 
  var getAllTables_path = url + "/getAllDataTable"; 
  var allCustomer_path = url + "/getCustomers";
  var generationCode_path = url + "/generationCode"; 
 
  $('#toggle-event').bootstrapToggle('off');
  $('#toggle-event').val("0");

  $('#toggle-event').change(function() {
    var form = $("#newWorkOrder");
    var toggle = $('#toggle-event');
    if(toggle.prop('checked') ){
      toggle.bootstrapToggle('disable');
      form.find('.action_number').attr('readOnly', false);
      $.ajax({
        url: generationCode_path,
        type: 'GET',
        dataType:'json',
        success: function(json)
        {
          var code = json.code;
          var msg = "VR";
          toggle.val("1");
          var select_1 = $("<option>").val(code).text("VR").attr("selected",true); 
          var select_2 = $("<option>").val(code).text("VR").attr("selected",true); 
          form.find(".waiter_dropdown").append(select_1);
          form.find(".table_dropdown").append(select_2);
          form.find(".name").val(msg);
          form.find("#customer_id").val(code);
          form.find(".current_action_number").val(code);
          form.find(".phone").val();
          form.find(".action_number").val(code);
          form.find(".direction").val(msg);
          form.find('.action_number').attr('readOnly', true);
          toggle.bootstrapToggle('enable');
          form.find('.one').trigger('click');
          toggle.bootstrapToggle('off');
        },
        complete: function(){
          form.find('.action_number').attr('readOnly', false);
        }
      }); 
    }
    else
    {
        form[0].reset();      
    }
  })

  var options = {
      url: getProducts_path,
      requestDelay: 200,
      getValue: function(element) {
        if ($('#input_product').val() == ''){
          $('#product_id').val('');
          $('#product_id').data('price',null);
          return '';
        }
        $('#product_id').val(element.id);
        $('#product_id').data('price',element.price);
        return element.name;
      },
      list: {
          match: {
              enabled: true
          },
          maxNumberOfElements: 8,
          // sort: {
          //     enabled: true
          // },
          // onSelectItemEvent: function (){
          //   $('#product_quantity')[0].focus()
          // },
          onHideListEvent: function() {
            // if ($('#input_product').val() != ''){
            //   $('#input_product')[0].blur();
            // }
            product_total();
          },
      },
  };
  $("#input_product").easyAutocomplete(options);


var options = {
  url: function(phrase) {
    return allCustomer_path;
  },
  getValue: function(element) {
    return element.name;
  },
  requestDelay: 200,
  list: {
    maxNumberOfElements: 4,
    match: {
      enabled: true
    },
    sort: {
      enabled: true
    },
    onClickEvent: function () {
      var action_number = $("#inputcustomer").getSelectedItemData().action_number;
      $("#newWorkOrder .action_number").val(action_number).trigger('keyup');
    },
    onHideListEvent:function(){
    },
  },
 template: {
    type: "description",
    fields: {
      description: "action_number"
    }
  },
  ajaxSettings: {
    dataType: "json",
    method: "get",
    data: {
      dataType: "json"
    }
  },
  preparePostData: function(data) {
    data.phrase = $("#inputcustomer").val();
    return data;
  },
};

$("#inputcustomer").easyAutocomplete(options);

$("#inputcustomer").change(function(){
    if ($('#inputcustomer').val() == ''){
      $('.current_action_number').val('');
      $('.action_number').val('');            
      $('.name').val('');
      $('#customer_id').val('');
      $('.direction').val('');
      $('.phone').val('');
      return '';
    }
});

//_________________________

var options = {
  url: function(phrase) {
    return allCustomer_path;
  },
  getValue: function(element) {
    return element.name;
  },
  requestDelay: 200,
  list: {
    maxNumberOfElements: 4,
    match: {
      enabled: true
    },
    sort: {
      enabled: true
    },
    onClickEvent: function () {
      var action_number = $("#customerinput").getSelectedItemData().action_number;
      $(".action_number_dash").val(action_number).trigger('keyup');
    },
    onHideListEvent:function(){
    },
  },
 template: {
    type: "description",
    fields: {
      description: "action_number"
    }
  },
  ajaxSettings: {
    dataType: "json",
    method: "get",
    data: {
      dataType: "json"
    }
  },
  preparePostData: function(data) {
    data.phrase = $("#customerinput").val();
    return data;
  },
};

$("#customerinput").easyAutocomplete(options);

$("#customerinput").change(function(){
    if ($('#customerinput').val() == ''){
      $('.current_action_number').val('');
      $('.action_number').val('');            
      $('.name').val('');
      $('.customer_id').val('');
      $('.direction').val('');
      $('.phone').val('');
      return '';
    }
});



  function refresh_dropdowns(){
    $.get(getTables_path, function (data) {
      $(".table_dropdown").html('').append('<option value=""></option>');
      $.each(JSON.parse(data),function(key, table) {
        if (table.status == 'Activa' && table.sales == "0"){
          $(".table_dropdown").append('<option value="'+table.id+'">' +table.number+ '</option>');
        }
      });

      try{
        $(".table_dropdown").not(':last').each(function (){
          $(this).find('option').first().next().attr('selected', true);
        })
      }catch (e){}
    }); 

    $.get(waiters_path, function (data) {
      $(".waiter_dropdown").html('').append('<option value=""></option>');
      $.each(JSON.parse(data),function(key, value) {
          $(".waiter_dropdown").append('<option value="'+value.id+'">' +value.name+ '</option>');
      }); 
    });
  }
  refresh_dropdowns();

  $.get(getAllTables_path, function (data) {
    $.each(data,function(key, table) {
      addTable(table);
    });
    tableScrollBottom();
  });

  $('#modal-danger-dashboard').on('show.bs.modal', function (e) { 
    $(".customer_dash").text($(".name_customer").text());
    $(".action_dash").text($(".action_number_dash").text());
    $(".price_dash").text($(".apagar").text());
  });

  function showReservation(e){

    var id = $(this).data('work_order_id');

    $.get(url + "/getTableData/"+ id, function (work_order) {

    if(work_order.status == "close")
    {
      $('#work_order_id').val(work_order.id);
      $('#facture_id_work').val(work_order.id);
      $('#code').val(work_order.code);
      $('span.table_id').text(work_order.table.number);
      $('span.table_id').data('table_id',work_order.table.id);
      $('.waiter_id').text(work_order.waiter_id);
      $('.description_work_order').text(work_order.description);
      $('#table_customer_id').val(work_order.customer_id);
      $('.sales').val(work_order.sales);

      if (work_order.customer ){
        
        if(work_order.sales == 1) {
          work_order.customer.action_number = "VR";
          $('.toggle_change_waiter').attr('disabled', true)
        }else{
          $('.toggle_change_waiter').attr('disabled', false)   
        }

        $('.name_customer').text(work_order.customer.name);
        $('.phone').text(work_order.customer.phone);
        $('.action_number').text(work_order.customer.action_number);
        $('.direction').text(work_order.customer.direction);
      }

      if (work_order.waiter){
        $('.name_waiter').text(work_order.waiter.name);
        //___________
        $('.percentage').val(work_order.waiter.percentage);
        //___________
      }

      if (work_order.table){
        $('.number_table').text(work_order.table.number);
        $('.number_table').data('table_id', work_order.table.id);
      }

      clearTotal()
      clearServices();

      $.each(work_order.service, function (key, service){
        addServices(service);
      });

      $.when($('#reservation').trigger('click')).done($('#input_product')[0].focus());

      $(".disabled").attr('disabled', "disabled");
      $(".disa").attr('disabled', "disabled");

    }
    else{
      $(".disabled").removeAttr('disabled', "disabled");
      $(".disa").removeAttr('disabled', "disabled");
      $(".info-box").removeClass('none');

      $('#work_order_id').val(work_order.id);
      $('#facture_id_work').val(work_order.id);
      $('#code').val(work_order.code);
      $('span.table_id').text(work_order.table.number);
      $('span.table_id').data('table_id',work_order.table.id);
      $('.waiter_id').text(work_order.waiter_id);
      $('.description_work_order').text(work_order.description);
      $('#table_customer_id').val(work_order.customer_id);
      // hidden 
      $('.sales').val(work_order.sales);

      if ( work_order.customer ){

        if (work_order.sales == 1) {
          work_order.customer.action_number = "VR";
          $('.toggle_change_waiter').attr('disabled', true)
        }else{
          $('.toggle_change_waiter').attr('disabled', false)   
        }

        $('.name_customer').text(work_order.customer.name);
        $('.phone').text(work_order.customer.phone);
        $('.action_number').text(work_order.customer.action_number);
        $('.direction').text(work_order.customer.direction);
      }
      if (work_order.waiter){
        $('.name_waiter').text(work_order.waiter.name);
        //___________
        $('.percentage').val(work_order.waiter.percentage);
        //___________
      }
      if (work_order.table){
        $('.number_table').text(work_order.table.number);
        $('.number_table').data('table_id', work_order.table.id);
      }
      clearTotal()
      clearServices();

      $.each(work_order.service, function (key, service){
        addServices(service);
      });

      $.when($('#reservation').trigger('click')).done($('#input_product')[0].focus());
      }

    });
  }

  $(".done_table").click(function(){
    $(".disabled").removeAttr('disabled', "disabled");
    $(".disa").removeAttr('disabled', "disabled");
  });
  $('#modal-danger-table').on('hide.bs.modal', function (e) { 
    $(".customer_table").text(" ");
    $(".action_table").text(" ");
  });
  
  $('#facture_id_work').click(function (){
    var work_order_id = $('#facture_id_work').val();
    showInvoice(work_order_id);
  });

  function clearServices(){
    $('#services').find('.product').not(':first').each(function (){
      $(this).remove();
    })
  }


  $('.done_product').click(function (){
    var item = $(this).data('item');
    item.find('.delete').trigger('delete');
  });

  $('.done_product_edit').click(function (){
    var item = $(this).data('item');
    item.find('.edit').trigger('edit');
  });
    

  function addServices(service)
  {
    var work_order_id = $('#work_order_id').val();
    var service_item = $('#original_product').clone(true);
    service_item.attr('id', 'service_'+service.id);

    var actual_price = parseFloat(service.actual_price)
    service_item.find('label.quantity').text(service.quantity);
    service_item.find('input.quantity').val(service.quantity);

    service_item.find('.total').text( (actual_price * service.quantity).toFixed(2))
    service_item.find('.price').text( (actual_price).toFixed(2))

    $('#services').append(service_item);
    service_item.fadeIn('fast');

    service_item.find('.delete').data('customer_id', $('#table_customer_id').val());
    service_item.find('.delete').data('service_id', service.id);
    service_item.find('.delete').data('work_order_id', work_order_id);
    service_item.find('.delete').data('price', actual_price);
    service_item.find('.delete').data('quantity', service.quantity);
    service_item.find('.delete').data('product_name', service.product.name);

    
    service_item.find('.edit').data('service_id', service.id);
    service_item.find('.edit').data('work_order_id', work_order_id);
    service_item.find('.edit').data('price', actual_price);
    service_item.find('.edit').data('quantity', service.quantity);
    service_item.find('.edit').data('product', service.product.name);


    service_item.find('.description').data('service_id', service.id);
    if (service.description){
      service_item.find('.description').attr("title", service.description)
    }

    if (service.product){
      service_item.find('.product_name').text(service.product.name);
    }

    waiter_percentage = service.waiter_percentage;

    service_item.find('.description').click(function(event){
      var item = $(this).parent().parent()
      var service_id = $(this).data('service_id');
      if (this.title != ''){
        alert(this.title);
      }
    });

    /*** DELETE SERVICE ***/
    $(".delete").click(function(e){
      var item = $(this).parent().parent();
      var name = $(this).data('product_name');
      $('#modal-danger-product').on('show.bs.modal', function (e) { 
          $(".name_product").text(name);
      });
      $('#modal-danger-product').on('hide.bs.modal', function (e) { 
          $(".name_product").text(" ");
      });

      $('.done_product').data('item', item);
    });

    service_item.find('.delete').on('delete',function (){
      var item = $(this).parent().parent()
      var service_id = $(this).data('service_id');
      var work_order_id = $(this).data('work_order_id');
      var price = parseFloat($(this).data('price')) * -1;
      var quantity = $(this).data('quantity');
      
      total(price, quantity);
      deleteService(service_id, item)
    });

    /*** EDIT SERVICE ***/
    $(".edit").click(function(e){
      item = $(this).parent().parent();
      var quantity = item.find('input.quantity').val();
      var product = $(this).data('product');
      $('.done_product_edit').data('item', item);

      $('#modal-danger-product_edit').on('show.bs.modal', function (e) { 
        $(".name_product_edit").text(product);
        item.find('input.quantity').val(quantity);
      });

      $('#modal-danger-product_edit').on('shown.bs.modal', function (e) { 
        if (flag_for_edit_services_from_form_new_services){
          $('.done_product_edit').trigger('click');
        }
      });
      $('#modal-danger-product_edit').on('hide.bs.modal', function (e) { 
        $(".name_product_edit").text(" ");
        item.find('input.quantity').val(quantity);
      });
      $('#modal-danger-product_edit').on('hidden.bs.modal', function (e) { 
        if (flag_for_edit_services_from_form_new_services){
          item.find('.accept').trigger('click');
        }
        flag_for_edit_services_from_form_new_services = false;
      });

    });
    
    service_item.find('.edit').on('edit',function (event){
      var item = $(this).parent().parent()
      var service_id = $(this).data('service_id');
      var work_order_id = $(this).data('work_order_id');
      item.find('button.accept').data('data', $(this).data());  
      item.find('button.delete').hide();
      item.find('button.edit').hide();
      item.find('label.quantity').hide();
      item.find('button.accept').show();
      item.find('input.quantity').show();
      item.find('input.quantity')[0].focus();

      item.find('input.quantity').keyup(function (e){
        if (e.keyCode == 13){
          $(this).off('keyup');
          item.find('button.accept').trigger('click');
          $(this).off('keyup');
        }
      });

    });

    service_item.find('button.accept').click(function (event){
      var item = $(this).parent().parent()
      var service_id = $(this).data('service_id');
      var work_order_id = $(this).data('work_order_id');
      
      var data = $(this).data('data');
      data.old_quantity = item.find('label.quantity').text() || 0;
      data.quantity = item.find('input.quantity').val() || 0;

      item.find('.delete').data('quantity', data.quantity)
      /** HIDE INPUT **/
      item.find('button.delete').show();
      item.find('button.edit').show();
      item.find('label.quantity').show();
      item.find('button.accept').hide();
      item.find('input.quantity').hide();
      editService(item, data);
      item.find('input.quantity').off('keyup');
    });    
    //CHANGE STATE WORK ORDER
    var table = $('#'+work_order_id).children().first();
    total(actual_price, service.quantity);
  }

  function editService(item, data){
    if (data.service_id > 0){
      data.id = data.service_id
      var submit_button = $(this).find('.submit_button');
      submit_button.attr('disabled', true)
      $.ajax({
        url: $('#_service_url').val() + '/' + data.service_id,
        headers: {'X-CSRF-TOKEN': $('#newService #_token').val()},
        type: 'PUT',
        cache: false,
        data: data,
        dataType: 'json',
        success: function(json)
        {
          if(json.success){
            toastr.success('Servicio Editado');
            // Inicio una animacion de parpadeo para saber cual item se edito
            $.when(
              $( item ).animate({
                opacity: 0.25,
              }, 500)
            ).done(function (){
              $( item ).animate({
                opacity: 1,
              }, 500)
            });

            var table = $('#'+json.service.work_order_id).children().first();
            switchWaitState(table);

            item.find('label.quantity').text(data.quantity);
            item.find('label.total').text( (data.price*data.quantity).toFixed(2) );
            var price = parseFloat(data.price);

            total(price, -data.old_quantity); // SUBTRACT
            total(price, data.quantity); // PLUS
          } 
        },
        error: function(data) 
        {
          var errors = data.responseJSON;
          $.each(errors.errors, function( key, value ) {
          toastr.error(value);
          return false;
          });  
        },
        complete: function (){
          submit_button.attr('disabled', false)
        }
      });
    }
  }



  $('#newService').submit(function (e){
    e.preventDefault();
    var data = $(this).serialize();
    var submit_button = $(this).find('.submit_button');
    submit_button.attr('disabled', true)
    $.ajax(
    {
      url: $('#_service_url').val(),
      headers: {'X-CSRF-TOKEN': $('#newService #_token').val()},
      type: 'POST',
      cache: false,
      data: data,
      dataType:'json',
      success: function(json)
      {
        if(json.success){
          if (json.update){
            // toastr.success('Servicio Editado Ya Existia');
            flag_for_edit_services_from_form_new_services = true;
            var service = $('#service_'+json.service.id);
            service.find('input.quantity').val(json.service.quantity);
            service.find('.edit').trigger('click');
          }
          else{
            toastr.success('Almacenado');
            addServices(json.service);
          }

          var table = $('#'+json.service.work_order_id).children().first();
          $("#newService")[0].reset();
          $('#customer_id').val('');
          $('#input_product').trigger('keyup').val('');
          $('#reservation').trigger('click');
          $('#product_total').val('');
          $('#product_id').data('price', 0).val('');
          switchWaitState(table);
        }
      },
      error: function(data) 
      {
        var errors = data.responseJSON;
        $.each(errors.errors, function( key, value ) {
          toastr.error(value);
          return false;
        });
      },
      complete: function (){
        submit_button.attr('disabled', false)
      }
    }); 
  })

  function addTable(table_data){
    if (table_data.isActive != '0'){
      var newtable = $('#original_table').clone(true);
      newtable.attr('id', table_data.id);
      var progress = "40%";

      newtable.data('work_order_id', table_data.id);
      switch(table_data.status){
        case 'wait':
          newtable.find('.info-box').addClass('bg-yellow');
          progress = "50%";
        break;
        case 'close':
          newtable.find('.info-box').addClass('bg-red');
          newtable.find('.info-box').addClass('none');
          newtable.find('.info-box').attr('data-target', '#modal-danger-table');
          newtable.find('.info-box').attr('data-toggle', 'modal');
          progress = "90%";
        break;
      }
      newtable.attr('tables', 'table_number_'+ 0);
      newtable.attr('customer_name', 'table_number_');

      var test = "*";
    
      if (table_data.customer){
        var name = table_data.customer.name
        if(name.indexOf(test) > -1) {
            newtable.find('.customer_name').text(table_data.customer.name).css("color","black");
        }
      }

      if(table_data.sales == 1)
      {
        if (table_data.customer){
          newtable.attr('tables', 'table_number_'+table_data.customer.action_number || 0);
          newtable.attr('customer_name', 'customer_name_'+table_data.customer.name || '');
          newtable.find('.customer_action_number').text("")
          newtable.find('.customer_name').text("VR")
        }
        if (table_data.table){
          newtable.find('.table_number').text("VR");
        }
      } 
      else
      {
        if (table_data.customer){
          newtable.attr('tables', 'table_number_'+table_data.customer.action_number || 0);
          newtable.attr('customer_name', 'customer_name_'+table_data.customer.name || '');
          newtable.find('.customer_action_number').text(table_data.customer.action_number)
          newtable.find('.customer_name').text(table_data.customer.name)
        }

        if (table_data.table){
          newtable.find('.table_number').text(table_data.table.number);
        }
      }


      newtable.find('.table_number').attr('id', 'table_number_'+table_data.id);

      //ATTACH EVENT CLICK

      newtable.click(function (){
        $('#modal-danger-table').on('show.bs.modal', function (e) { 
          if (table_data.customer){
            $(".customer_table").text(table_data.customer.name);
            $(".action_table").text(table_data.customer.action_number);
          }
        });
      });

      newtable.click(showReservation);

      $('#tables').append(newtable);
      newtable.fadeIn('slow', function (){
        newtable.find('.progress-bar').css('width', progress);
      });

      return newtable;
    }
  }

  function tableScrollBottom(){
    return $.when( $('.list .box-body').animate({scrollTop:$('.list .box-body')[0].scrollHeight }, 1000) );
  }

  $('#newWorkOrder').submit(function (e){
    var data = $('#newWorkOrder').serialize();
    var submit_button = $(this).find('.submit_button');
    submit_button.attr('disabled', true)
    e.preventDefault();
    $.ajax(
    {
      url: $('#newWorkOrder #_url2').val(),
      headers: {'X-CSRF-TOKEN': $('#newWorkOrder #_token').val()},
      type: 'POST',
      cache: false,
      data: data,   
      success: function(response)
      {
        if(response.success){
          toastr.success('Almacenado');
          $("#newWorkOrder")[0].reset();
          $('#customer_id').val('');

          var table = addTable(response.work_order);
          table.trigger('click');

          tableScrollBottom();
          refresh_dropdowns();
        }else{
          toastr.error('El cliente ingresado ya tiene una mesa registrada');
        }
      },
      error: function(data) 
      {
        var errors = data.responseJSON;
        $.each(errors.errors, function( key, value ) {
          toastr.error(value);
          return false;
        });
      },
      complete: function (){
        submit_button.attr('disabled', false)
      }
    });  
    return false;
  });
  //mostrar los datos del usuario, mesero, mesa al presionar encima de alguna ventana


  function deleteService(id, item){

    var url = $('#_service_url').val();
    url = url +"/"+ id;
    $.ajax(
    {
      url: url,
      headers: {'X-CSRF-TOKEN': $('#newService #_token').val()},
      type: 'DELETE',
      cache: false,
      dataType: 'json',
      success: function(response){
        if (response.success){
          toastr.success('Servicio Elminado');
          // Inicio una animacion de parpadeo para saber cual item se edito
          $.when(
            $( item ).animate({
              opacity: 0.25,
            }, 250)
          ).done(
            function (){
              $( item ).animate({
                opacity: 1,
              }, 250, function (){
              $(item).remove();
            })
          });
          var table = $('#'+response.id).children().first();
          switchWaitState(table);
        }
      },
      error: function(response) 
      {
      }
    });

    return false;
  }

  function switchWaitState(table){
    table.removeClass('bg-red').addClass('bg-yellow').first().removeAttr('data-target', '#modal-danger-table').removeAttr('data-toggle', 'modal');
     ;
    table.find('.progress-bar').css('width', '50%');
    return table
  }

  function switchCloseState(table){
    table.removeClass('bg-red').addClass('bg-red');
    table.find('.progress-bar').css('width', '90%');
    return table
  }


  /*** PRODUCT TOTAL ***/
  function product_total (){
    var price = parseFloat($('#product_id').data('price')) || 0;
    var quantity = parseFloat($('#product_quantity').val()) || 0;
    var total = (price * quantity).toFixed(2);
    $('#product_total').val(total);
  }

  $('#input_product').keyup(product_total);
  $('#product_quantity').keyup(product_total);
  
  $('.action_number').keyup(function(){

    var form = $(this).parent().parent().parent();
    var action_number = $(this).val();
    var url = $('#_url').val(); 
    var actionCustomer_path = url + "/actionCustomer"; 
    var url = actionCustomer_path+ "/" + action_number; 
    if (action_number.length > 1){
      $.ajax({
        type: "GET",
        url:url,
        data: "action_number="+action_number,
        dataType: "JSON",
        error: function(data){
        },
        success: function(response){
          if (response.success){
            if (form.find('#customer_id').length > 0){
              form.find('#customer_id').val(response.customer.id);
            }
            if (form.find('.customer_id').length > 0){
              form.find('.customer_id').val(response.customer.id);
            }            
            form.find('.current_action_number').val(response.customer.action_number);
            form.find('.name').val(response.customer.name);
            form.find('.direction').val(response.customer.direction);
            form.find('.phone').val(response.customer.phone);
          }else if (action_number == form.find('.action_number').val() ){
            if (form.find('#customer_id').length > 0){
              form.find('#customer_id').val('');
            }

            if (form.find('.customer_id').length > 0){
              form.find('.customer_id').val('')
            }
          }

          if (!response.success){
            form.find('.current_action_number').val('');
            form.find('.name').val('');
            form.find('.direction').val('');
            form.find('.phone').val('');
          }

        }
      });             
    }
  });


  $('#toggle_change_work_order').click(function (){
    var select = $('#change_work_order');
    if ( !select.is(':visible') ){
      select.show();
      select.find('option').first().prop(':selected');
      select[0].focus();
    }else{
      select.hide()
    }
  });

  $('#change_work_order').change(function (){
    var work_order_id = $('#work_order_id')
    var new_table_id = $(this).val();
    var new_table_number = $("#change_work_order option:selected").text();

    data = {
      work_order_id: work_order_id.val(),
      table_id: new_table_id,
      status: 'Inactiva', 
      old_table_id: $('span.table_id').data('table_id')
    }

    if (data.new_table_id != ''){
      $.ajax({
        url: $('#newWorkOrder #_url2').val() + '/' + data.work_order_id,
        headers: {'X-CSRF-TOKEN': $('#newService #_token').val()},
        type: 'PUT',
        cache: false,
        data: data,
        dataType: 'json',
        success: function(json)
        {
          if(json.success){
            toastr.success('Mesa Cambiada');
            $('#toggle_change_work_order').trigger('click');
            $('span.table_id').text(new_table_number);
            $('#table_number_'+data.work_order_id).text(new_table_number);
            var table = $('#'+data.work_order_id).children().first();
            switchWaitState(table);
          } 
        },
        error: function(data) 
        {
          var errors = data.responseJSON;
          $.each(errors.errors, function( key, value ) {
          toastr.error(value);
          return false;
          });  
        },
        complete: function (){
            refresh_dropdowns();
        }
      });
    }
  });

  //CHANGE CODE

  /*$(".radio-code").change(function(){ if($(this).is(':checked')) {$(this).val("1");} else {$(this).val("0");} });

  $('.radio-code').change(function (){

    var work_order_id = $('#work_order_id').val();
    var code = $(this).val();

    data = {
      work_order_id: work_order_id,
      code: code,
    }

      $.ajax({
        url: $('#_url').val() + '/changeCode/' + data.work_order_id,
        headers: {'X-CSRF-TOKEN': $('#newService #_token').val()},
        type: 'PUT',
        cache: false,
        data: data,
        dataType: 'json',
        success: function(json)
        {
          if(json.success){
            console.log("exito");
          } 
        },
        error: function(data) 
        {
          var errors = data.responseJSON;
          $.each(errors.errors, function( key, value ) {
          toastr.error(value);
          return false;
          });  
        },
        complete: function (){
            //refresh_dropdowns();
        }
      });
  });
*/
  //CHANGE WAITER 

  $('#toggle_change_waiter').click(function (){
    var select = $('#change_waiter');
    if ( !select.is(':visible') ){
      select.show();
      select.find('option').first().prop(':selected');
      select[0].focus();
    }else{
      select.hide()
    }
  });
  
 $('#change_waiter').change(function (){
    var work_order_id = $('#work_order_id')
    var new_waiter_id = $(this).val();
    var new_waiter = $("#change_waiter option:selected").text();

    data = {
      work_order_id: work_order_id.val(),
      waiter_id: new_waiter_id,
    }

    if (data.new_waiter_id != ''){
      $.ajax({
        url: $('#newWorkOrder #_url2').val() + '/' + data.work_order_id,
        headers: {'X-CSRF-TOKEN': $('#newService #_token').val()},
        type: 'PUT',
        cache: false,
        data: data,
        dataType: 'json',
        success: function(json)
        {
          if(json.success){
            toastr.success('Mesonero Cambiado');
            $('#toggle_change_waiter').trigger('click');
            $('span.name_waiter').text(new_waiter);
            var table = $('#'+data.work_order_id).children().first();
            switchWaitState(table);
          } 
        },
        error: function(data) 
        {
          var errors = data.responseJSON;
          $.each(errors.errors, function( key, value ) {
          toastr.error(value);
          return false;
          });  
        },
        complete: function (){
            refresh_dropdowns();
        }
      });
    }
  });




    $('.done').click(function(){              
    
    var work_order_id = $('#work_order_id')
    
    data = {
      work_order_id: work_order_id.val(),
      status: 'Activa', 
      old_table_id: $('span.table_id').data('table_id'),
      table_id: $('span.table_id').data('table_id'),
      isActive: '0'
    }

    if (data.new_table_id != ''){
      $.ajax({
        url: $('#newWorkOrder #_url2').val() + '/' + data.work_order_id,
        headers: {'X-CSRF-TOKEN': $('#newService #_token').val()},
        type: 'PUT',
        cache: false,
        data: data,
        dataType: 'json',
        success: function(json)
        {
          if(json.success){
            toastr.success('Mesa Cerrada');
            $('#newService').find('td span').slice(0,5).each(function (){
              $(this).text('');
            });
            $('#table_number_'+data.work_order_id).parent().parent().parent().remove();
            clearServices();
            clearTotal();
            refresh_dropdowns();
          } 
        },
        error: function(data) 
        {
          var errors = data.responseJSON;
          $.each(errors.errors, function( key, value ) {
          toastr.error(value);
          return false;
          });  
        },
        complete: function (){
            refresh_dropdowns();
        }
      });
    }
  });


  /******** MOVE SERVICES *************/
  $('#moveServices').click(function (){
    clearServicesToMove();
    $('#services').find('.product').not('#original_product').each(function (){
      var service = $(this);
      var data = service.find('.delete').data();
      data.product_name = service.find('.product_name').text();
      addServicesToMove(data);
    })
    activeICheck();
    $('#toggle_move_reservation').trigger('click');
  })

  function addServicesToMove(data){
    var product_to_move = $('#original_services_to_Move').clone(true);
    product_to_move.show();
    $('#moveWorkOrder .customer_id').val(data.customer_id);
    product_to_move.attr('id', 'product_to_move_'+data.service_id);
    product_to_move.find('.service_id').val(data.service_id);
    product_to_move.find('.quantity').text(data.quantity);
    product_to_move.find('.product_name').text(data.product_name);
    product_to_move.find('.price').text( (data.price * data.quantity).toFixed(2) );

    $('#services_to_move').append(product_to_move);
  }

  function clearServicesToMove(){
    $('#services_to_move').find('.product').not('#original_services_to_Move').each(function (){
      var service = $(this);
      service.remove();
    })
  }

  $('#moveWorkOrder').submit(function (e){
    e.preventDefault();
    var submit_button = $(this).find('.submit_button');
    submit_button.attr('disabled', true)
    var data = $(this).serialize();
    var length_services = $(this).find('.product').not('#original_services_to_Move').find('input:checked').length;
    if (length_services > 0){
      $.ajax(
      {
        url: $('#moveWorkOrder #_url').val() + '/' + 'moveWorkOrder',
        headers: {'X-CSRF-TOKEN': $('#moveWorkOrder #_token').val()},
        type: 'POST',
        cache: false,
        data: data,
        dataType: 'json',
        success: function(json)
        {
          if(json.success){
            toastr.success('Almacenado');
            $("#moveWorkOrder")[0].reset();
            $('.customer_id').val('');
            clearServicesToMove();
            var table = addTable(json.work_order);
            table.trigger('click');
            tableScrollBottom();
            refresh_dropdowns();
          }else{
          toastr.error('El cliente ingresado ya tiene una mesa registrada');
          }
        },
        error: function(data) 
        {
          var errors = data.responseJSON;
          $.each(errors.errors, function( key, value ) {
            toastr.error(value);
            return false;
          });
        },
        complete: function (){
          submit_button.attr('disabled', false)
        }
      });
    }
    else{
      toastr.error("debe seleccionar algún servicio");
    }

  })

  $('#cancel_move').click(function (){
    clearServicesToMove();
    $("#moveWorkOrder")[0].reset();
    $('#reservation').trigger('click');
  })

});

/** CALC TOTAL ***/
function total (price, quantity){

  var subtotal      = $('#subtotal');
  var service_total = $('#service_total');
  var node_total    = $('#total');
  var node_iva    = $('#iva');

  var total_iva = 0;
  var price = parseFloat(price) || 0;
  var quantity = parseFloat(quantity) || 0;
  var sum = price * quantity;
  var current_total = parseFloat(subtotal.text()) || 0;
  var total = sum + current_total;
  if (iva > 0){
    total_iva = total * iva / 100;
  }

  var service = (total * waiter_percentage) / 100;

  subtotal.text(total.toFixed(2));
  if (iva > 0){
    $(".iva").show();
    node_iva.text(total_iva.toFixed(2));
  }
  service_total.text(service.toFixed(2));
  node_total.text((total + service + total_iva).toFixed(2));
}

function clearTotal (){
  $('#subtotal').text('0'); 
  $('#service_total').text('0');
  $('#total').text('0');
}

$('#search').keyup(function (e){
  var text = $(this).val().toUpperCase();

  var searching = '#tables [tables*="table_number_'+ text +'"]' + ',' + '#tables [customer_name*="customer_name_'+ text +'"]';
  if (e.keyCode == 13){
    var table = $(searching).first();
    if (table.is(':visible')){
      table.trigger('click');
    }
    $(this).val('');
    // return false;
  }

  $('#tables > div').not('#original_table').each(function (){
    $(this).hide();
  });

  $(searching).each(function (key){
    $(this).show();
  });

});
