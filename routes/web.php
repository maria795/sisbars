<?php

Auth::routes();

Route::middleware(['auth',])->group(function () {

    Route::resource('backup', 'backupController');
    Route::resource('products','admin\ProductsController');
    Route::resource('waiters','admin\WaitersController');
    Route::resource('tables','admin\TablesController');
    Route::resource('customers','admin\CustomersController');
    Route::resource('dashboard','admin\WorksOrdersController');
    Route::resource('services','admin\ServicesController');
    Route::resource('configuration','admin\ConfigurationController');
    Route::resource('user', 'UserController');
    Route::resource('logins', 'LoginController');
    Route::resource('permission', 'PermissionController');
    Route::get('/', 'admin\WorksOrdersController@index')->name('home');
    Route::get('reports', 'ReportsController@index');
    Route::get('getAllDataTable', 'admin\WorksOrdersController@getAllDataTable');
    Route::get('data', 'admin\WorksOrdersController@getDataReport');
    Route::get('about','admin\AboutController@index');
    Route::get('dataRange', 'admin\WorksOrdersController@ReportDateRange');
    Route::get('generationCode', 'admin\WorksOrdersController@generationCode');
    Route::get('getActiveTables','admin\TablesController@getActiveTables');
    Route::get('getProducts','admin\ProductsController@indexGet');
    Route::get('getWaiters','admin\WaitersController@indexGet');
    Route::get('getTables','admin\TablesController@indexGet');
    Route::get('getCustomers','admin\CustomersController@indexGet');
    Route::get('actionCustomer/{code}','admin\CustomersController@codeCustomer');
    Route::get('nameCustomer/{name}','admin\CustomersController@nameCustomer');
    Route::get('getBackup', 'backupController@indexGet');
    Route::get('facture/{id}','admin\ConfigurationController@getTableDataFacture');
    Route::get('getTableData/{id}','admin\WorksOrdersController@getTableData');
    Route::post('moveWorkOrder', 'admin\WorksOrdersController@moveWorkOrder');
    Route::put('getStatusWorkOrder/{id}','admin\WorksOrdersController@getWorkOrderStatus');
    Route::put('changeCode/{code}','admin\WorksOrdersController@changeCode');
    Route::delete('deleteWorkOrder/{id}','admin\WorksOrdersController@delete');

});
  

Route::get('refresh_captcha', 'Auth\LoginController@refreshCaptcha')->name('refresh_captcha');  
Route::get('reset', 'AnswerController@showAnswerRequestForm')->name('password.request');
Route::Post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/resets', 'Auth\ResetPasswordController@reset')->name('password.resets');
Route::get('response', 'AnswerController@index');
Route::get('verification', 'AnswerController@verificationAnswer');
