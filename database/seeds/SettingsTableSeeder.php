<?php
use Illuminate\Database\Seeder;
use App\Models\Configuration;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $configuration = new Configuration;
        $configuration->id = 1;
        $configuration->name  = "RESTAUTANTE 'LA PROA'";
        $configuration->description = "CLUB PUERTO AZUL";
        $configuration->iva = "14";
        $configuration->status = "SI";
        $configuration->save();
    }
}
