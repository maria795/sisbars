<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->id = '1';
        $user->name = 'usuario';
        $user->last_name = 'administrador';
        $user->question = '¿Cómo me llamo?';
        $user->answer = 'Admin';
        $user->email = 'admin@mail.com';
        $user->password = 'admin';
        $user->status = 1; // (1) active (0)disabled
        $user->save();
        $user->assignRole('admin');

    }
}
