<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesAndPermissionsSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(SettingsTableSeeder::class);
    
        //factory('App\Models\Product',50)->create();
        //factory('App\Models\Customer',50)->create();
        //factory('App\Models\Table','ACTIVA', 50)->create();
       // factory('App\Models\Table','INACTIVA', 50)->create();
    }
}
