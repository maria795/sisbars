<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdditionalTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tables', function(Blueprint $table){
            $table->increments('id');
            $table->string('number');
            $table->enum('status', ['Activa','Inactiva']);
            $table->enum('sales', [0, 1])->default(0);
            $table->timestamps();

        });
        
        Schema::create('waiters', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->float('percentage')->unsigned();
            $table->enum('sales', [0, 1])->default(0);
            $table->timestamps();

        }); 

       Schema::create('customers', function(Blueprint $table){
            $table->increments('id');
            $table->string('action_number');
            $table->string('name')->default(" ");
            $table->string('phone')->nullable()->default(" ");
            $table->string('direction')->nullable()->default(" ");
            $table->enum('sales', [0, 1])->default(0);
            $table->timestamps();
        });

        Schema::create('products', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('description')->nullable()->default(" ");
            $table->decimal('price', 20, 2)->unsigned();
            $table->timestamps();
        });

        Schema::create('configurations', function(Blueprint $table){
            $table->increments('id');
            $table->string('name')->nullable()->default(" ");
            $table->string('description')->nullable()->default(" ");
            $table->string('iva')->nullable()->default(" ");
            $table->enum('status', ['SI','NO']);
            $table->timestamps();
        });

       Schema::create('works_orders', function(Blueprint $table){
            $table->increments('id');
            $table->integer('code')->unsigned()->nullable();
            $table->integer('customer_id')->unsigned()->nullable();
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('SET NULL');
            $table->integer('table_id')->unsigned()->nullable();
            $table->foreign('table_id')->references('id')->on('tables')->onDelete('SET NULL'); 
            $table->integer('waiter_id')->unsigned()->nullable();
            $table->foreign('waiter_id')->references('id')->on('waiters')->onDelete('SET NULL'); 
            $table->string('description')->nullable()->default(" ");
            $table->enum('status', ['open', 'wait','close']);
            $table->enum('isActive', ['1', '0'])->nullable()->default('1');
            $table->enum('sales', [0, 1])->default(0);
            $table->timestamps();
       });

        Schema::create('services', function(Blueprint $table){
         $table->increments('id');
         $table->integer('work_order_id')->unsigned();   
         $table->foreign('work_order_id')->references('id')->on('works_orders')->onDelete('CASCADE');;  
         $table->integer('code')->unsigned()->nullable();
         $table->integer('product_id')->unsigned()->nullable();
         $table->foreign('product_id')->references('id')->on('products')->onDelete('SET NULL');      
         $table->string('description')->nullable()->default(" ");
         $table->float('actual_price')->unsigned();         
         $table->float('quantity')->unsigned();
         $table->float('waiter_percentage')->unsigned();
         $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
        Schema::dropIfExists('products');
        Schema::dropIfExists('waiters');
        Schema::dropIfExists('works_orders');
        Schema::dropIfExists('services');
        Schema::dropIfExists('backups');
    }
}