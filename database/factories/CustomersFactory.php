<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Customer::class, function (Faker $faker) {
    return [
        'action_number' => $faker->randomNumber(4),
        'name' => $faker->name,
        'phone' => $faker->phoneNumber,
        'direction' => $faker->address,
    ];
});
