<?php

use Faker\Generator as Faker;

$factory->defineAs(App\Models\Table::class, "ACTIVA", function ($faker) {
    return [
        'number' => $faker->randomNumber(2),
        'status' => "ACTIVA",
    ];
});
