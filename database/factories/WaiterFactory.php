<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Waiter::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'percentage' => $faker->randomNumber(2),
    ];
});
