# Welcome to Sisbar!

## Spanish Documentation
Para probar Sisbar necesita seguir los siguientes pasos:

* tener docker instalado en su computador
	* Ejemplo : Para distribuciones basadas en Debian
		```
		$ sudo apt install docker
		```

*  Si su instalación de Docker no tiene docker-compose: 
	```
	$ sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
	```
	* Para probar si lo tiene instalado intente 
		```
		$ docker-compose -v
		```	
*  Ejecutar el siguiente y ultimo comando:
	```
	$ docker-compose up --build -d
	```
* Esperar que termine el proceso y abrir el navegador en las siguientes direcciones:
	* localhost (Dirección donde se encuentra Sisbar)
	* localhost:8000 (Dirección donde se encuentra PHPMyAdmin) }

> **Los Datos del usuarios y la contraseña** se encuentran en el archivo docker-compose.yml abrirlo para buscar el nombre de las variables


## Documentation
Sisbar Requirements:

* have  docker installed  in your computer or server
	* example : for Debian Based Distributions
		```
		$ sudo apt install docker
		```

*  if you don't have to installed docker-compose: 
	```
	$ sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
	```
	* Try docker-compose
		```
		$ docker-compose -v
		```	
*  Run the last command:
	```
	$ docker-compose up --build -d
	```
* wait the process finish and open browser to next urls:
	* localhost (Sisbar direction)
	* localhost:8000 (PHPMyAdmin direction) 


> **the password and user is in the file docker-compose** Open this file is  useful for you
